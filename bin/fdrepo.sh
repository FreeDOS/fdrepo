#!/bin/bash

# echo "Testing"

## FreeDOS Software Repository Maintenance Utility Script, 2nd Edition
## Copyright 2017-2021 Jerome E. Shidel Jr.
## Released under GNU General Public License, Version 2
##
## usage: fdrepo.sh [options]
##
##     help         Display help screen and exit
##
##     [no]force    Enable/disable update forcing. Being forceful is a bad idea.
##                  This will take a really long time. So don't use it. Ever!
##                  (Unless something isn't working.)
##
##     quiet        Turn off chatter and only show error messages.
##
##     verbose      This is the default. So, I don't know why you need it.
##
## Repository routines:
##
##     repo ID      Select repository by ID. (if this is not provided, the
##                  default repository is automatically selected)
##
##     activate     Activate the current repository as the default.
##
##     available    Display a list of available repositories.
##
## General repository maintenance routines:
##
##     update       Adjust all lowercase names, permissions, links, metadata
##                  reference files, html pages and blah blah.
##
##     cleanup      Remove obsolete data and html reference files and such.
##
##     all          Perform all maintenance routines. When you have to much time
##                  on your hands, use this instead of just running "update".
##
## Additional repository routines:
##
##     csv          Recreate listing.csv index file. (Updated automatically
##                  when already present) (Slow)
##
##     rss          Update rss.xml update feed. (Updated automatically
##                  when already present)
##
##     compare IDs  (Re)create a comparison.html page for the current repository
##                  in the html directory. Repository IDs listed will be compared
##                  in order. If you want the current repository in the table,
##                  it must also be in the ID list. ( Something like
##                  "1.0,1.1,1.2..." ) (Slow)
##
##     purge        This will delete all utility created files and any links that
##                  are located within the repository. It is dangerous if any
##                  files used to build a CD ISO exist within the repository.
##                  This should only be used as a last resort if/when something
##                  goes very wrong. It always requires the "force" directive.
##                  It will then require running "update" or "all" and maybe
##                  "csv", "rss", "compare" and "iso" to completely rebuild.
##
## CD ISO creation routines (Slow):
##
##     iso          Same as cdiso
##
##     cdiso        Create legacy el-torito based boot cd.
##
##     altiso       Create alternate isolinux+memdisk based boot cd.
##
## Package and file management routines: (Note: You should run update soon
## after performing any of these actions. These do not update the METADATA,
## HTML, CSV or XML files)
##
##     add ID FILES Add package FILES to the repository into group ID.
##
##     edit FILES   Edit the lsm meta data for a package FILES (or ID of the
##                  latest version in the repository).
##
##     move ID PKGS Move specified packages in the repository into group ID.
##

# Special thanks to the bash IRC on freenode for catching some bugs and
# suggesting

# Display General Information and License
function show_about
{
    grep -a  "^##" "${SELF}" | grep -a  -m 1 -B 100 "^##$" | grep -a  -iv "^##$" | cut -c 4-
    exit 0
}

# Display Help Screen and exit
function show_help
{
    grep -a  "^##" "${SELF}" | cut -c 4-
    exit 0
}

# Display error message and exit with error code
function show_error
{
    local code=${1}
    shift
    log "ERROR ($code): ${@}"
    exit $code
}

# Variable based External function processor
function external
{
    local name="FUNCTION_${1}"
    shift
    [[ "${!name}" == "" ]] && show_error 1 "function \`$name' not assigned"
    if [[ "${!name//\$1/}" != "${!name}" ]] ; then
        eval "${!name}"
    else
        eval "${!name} $@"
    fi
    return $?
}

# Normal display message to standard output device
function message
{
    [[ "$QUIET" == true ]] && return 0
    echo "${SPACES:1:$INDENT}${@}" >&2
}

# Display and log a message
function log
{
    message "$@"
    [[ "$ANYLOGGING" == "" ]] && {
        ANYLOGGING=true
        local msg="$(external DATELOG) -- Repository \`$REPOSITORY_TITLE' ($REPOSITORY_ID)"
        if [[ "$FILE_LOG" != "" ]] ; then
            echo "${msg} ${STARS:0:$(( ${#STARS} - ${#msg} - 1))}">>"$FILE_LOG"
        else
            echo "${msg} ${STARS:0:$(( ${#STARS} - ${#msg} - 1))}">&2
        fi
    }
    [[ "$FILE_LOG" != "" ]] && {
        echo "$(external DATELOG) -- ${@}">>"$FILE_LOG"
    }
}

# Sanitize Filename
function safe_filename
{
    local n="${1//[\{\}]/}"
    n="${n//[![:alnum:]._()-]/_}"
    echo "${n//+(_)/_}"
    # echo "$1 as ${n//+(_)/_}" >&2
}

# Set configuration to defaults
function config_default
{
    QUIET=false
    FORCE=no
    INDENT=80

    MAX_WIDTH=2048
    MAX_RSS=200
    MAX_RSS_NEW=10

    PATH_TEMP="${HOME}/tmp"
    FILE_LOG=""

    REPOSITORY=''
    SERVERNAME=$(hostname)
    CONTACT="someone@somewhere.com"
    WEBMASTER="Nobody"

    PATH_DATA="pkg-info"
    PATH_HTML="pkg-html"

    PATH_UPDATES=""

    PREFIX_GROUP="group-"

    EXTENSION_DATA="txt"
    EXTENSION_HTML="html"
    EXTENSION_XML="xml"

    COMPARE_DELETED="yes"

    FILE_CD_ISO="cdrom.iso"
    FILE_CD_FLOPPY="boot.img"
    FILE_CD_FILES=""
    FILE_ALT_FLOPPY="boot.img"
    FILE_ALT_ISO="alternate.iso"
    FILE_ALT_FILES="${SELF}/fdrepo.isolinux.tgz"

    FILTER_META=""
    FILTER_LST="Build time:"
    FILTER_TXT="Build time:"
    FILTER_CSS=""
    FILTER_HTML="last-updated"
    FILTER_XML="<lastBuildDate>\|<pubDate>"

    FUNCTION_DATELOG='date +"%Y-%m-%d @ %H:%M:%S"'
    FUNCTION_DATEMOD='date +"%Y-%m-%d"'
    FUNCTION_DATEYEAR='date +"%Y"'
    FUNCTION_DATERSS='date -u +"%a, %d %b %Y %H:%M:%S +0000"'
    FUNCTION_FILESIZE='echo $(ls -lh "$1" 2>/dev/null || echo "0 0 0 0 ?") | cut -d " " -f 5'
    FUNCTION_BYTESIZE='echo $(ls -l "$1" 2>/dev/null || echo "0 0 0 0 ?") | cut -d " " -f 5'
    FUNCTION_DATESTAMP='date'
    FUNCTION_BUILDIDX="buildidx $1 >/dev/null"

    FUNCTION_EDIT='vi "$1"'

    VERSIONING='.'

    if [[ "$MacOSX" == "yes" ]] ; then
        FUNCTION_STAT='stat -f %m $1 2>/dev/null'
        FUNCTION_CRC='crc32'
        FUNCTION_MD5='md5 $1 | cut -d "=" -f 2 | cut -d " " -f 2'
        FUNCTION_SHA='shasum -a 256 $1 | cut -d " " -f 1'
        FUNCTION_EPOCH_TO_DATE='date -r $1'
        FUNCTION_CDISO='hdiutil makehybrid -quiet -o ../cdrom.iso -eltorito-boot boot.img -iso -default-volume-name $(date +"%Y%m%d")FDREPOCD .'
        FUNCTION_ALTISO='hdiutil makehybrid -quiet -o ../cdrom.iso -eltorito-boot boot.img -iso -default-volume-name $(date +"%Y%m%d")FDREPOCD .'
        FUNCTION_CDTITLE='echo Repository CD \($(date +"%Y-%m-%d")\)'
        FUNCTION_ALTTITLE='echo Repository CD \($(date +"%Y-%m-%d")\)'
    else
        FUNCTION_STAT='stat --format %Y $1 2>/dev/null'
        FUNCTION_CRC=''
        FUNCTION_MD5='md5sum $1 | cut -d " " -f 1'
        FUNCTION_SHA='sha256sum $1 | cut -d " " -f 1'
        FUNCTION_EPOCH_TO_DATE='date -d @$1'
        FUNCTION_CDISO='mkisofs -input-charset utf-8 -V $(date +"%Y%m%d")FDREPOCD -r -f -b boot.img -c boot.cat -o ../cdrom.iso .'
        FUNCTION_ALTISO='mkisofs -o ../cdrom.iso -V $(date +"%Y%m%d")FDREPOCD -r -b ISOLINUX/ISOLINUX.BIN -c ISOLINUX/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table CDROOT'
        FUNCTION_CDTITLE='echo Repository CD \($(date +"%Y-%m-%d")\)'
        FUNCTION_ALTTITLE='echo Repository CD \($(date +"%Y-%m-%d")\)'
    fi;

    FILE_IGNORE=";index.lst;index.gz;"
}

# Read the configuration file
function config_read
{
    CONFIG=
    TEMPLATE=

    local base
    local this
    local i


    base="${SELF##*/}"
    base="${base%.*}"

    # Check and load the normal installation directory config file
    [[ -f "/opt/fdrepo/${base}.config" ]] && {
        CONFIG="/opt/fdrepo/${base}.config"
        . "${CONFIG}"
    }

    # Check and load the exec path config file
    [[ -f "${SELF%.*}.config" ]] && {
        CONFIG="${SELF%.*}.config"
        . "${CONFIG}"
    }

    # Check and load the current directory config file
    [[ -f "${PWD}/${base}.config" ]] && {
        CONFIG="${PWD}/${base}.config"
        . "${CONFIG}"
    }

    # Check and load the user's home directory config file
    [[ -f "${HOME}/${base}.config" ]] && {
        CONFIG="${HOME}/${base}.config"
        . "${CONFIG}"
    }

    # If not set configure for home directory
    [[ "$CONFIG" == "" ]] && CONFIG="${HOME}/${base}.config"

    # Set the template file location
    [[ -f "/opt/fdrepo/${base}.template" ]] && TEMPLATE="/opt/fdrepo/${base}.template"
    [[ -f "${SELF%.*}.template" ]] && TEMPLATE="${SELF%.*}.template"
    [[ -f "${PWD}/${base}.template" ]] && TEMPLATE="${PWD}/${base}.template"
    [[ -f "${HOME}/${base}.template" ]] && TEMPLATE="${HOME}/${base}.template"
    [[ "$TEMPLATE" == "" ]] && TEMPLATE="${HOME}/${base}.template"

    i=0
    while [[ "${GROUP_DATA[$i]}" != "" ]] ; do
        this="${GROUP_DATA[$i]}"
        GROUP_ID[$i]="${this%%,*}"
        this="${this:$(( ${#GROUP_ID[$i]} + 1))}"
        GROUP_TITLE[$i]="${this%%,*}"
        this="${this:$(( ${#GROUP_TITLE[$i]} + 1))}"
        GROUP_DESCRIPTION[$i]="${this}"
        (( i++ ))
    done

    i=0
    while [[ "${META_DATA[$i]}" != "" ]] ; do
        this="${META_DATA[$i]}"
        LSM_ID[$i]="${this%%,*}"
        this="${this:$(( ${#LSM_ID[$i]} + 1))}"
        LSM_HANDLER[$i]="${this%%,*}"
        this="${this:$(( ${#LSM_HANDLER[$i]} + 1))}"
        LSM_TITLE[$i]="${this}"
        (( i++ ))
    done

    [[ "${PATH_TEMP}" == "" ]] && show_error 1 "Temporary path not defined"
}

function show_config
{

    local i
    local data

    echo "# FreeDOS Software Repository Maintenance Utility Script Configuration File"
    echo
    echo "# The server's identification name"
    echo "SERVERNAME=\"${SERVERNAME}\""
    echo
    echo "# Webmaster Email"
    echo "CONTACT=\"${CONTACT}\""
    echo
    echo "# Webmaster Name"
    echo "WEBMASTER=\"${WEBMASTER}\""
    echo
    echo "# The default/current repository identification label"
    echo "REPOSITORY=\"${REPOSITORY}\""
    echo
    echo "# Directory for temporary files and things"
    echo "PATH_TEMP=\"${PATH_TEMP}\""
    echo
    echo "# Log file"
    echo "FILE_LOG=\"${FILE_LOG}\""
    echo
    echo "# Sub-directory for static LSM meta data files"
    echo "PATH_DATA=\"${PATH_DATA}\""
    echo
    echo "# Sub-directory for static HTML pages"
    echo "PATH_HTML=\"${PATH_HTML}\""
    echo
    echo "# Directory that can contain existing package updates"
    echo "PATH_UPDATES=\"${PATH_UPDATES}\""
    echo
    echo "# Prefix for HTML group files"
    echo "PREFIX_GROUP=\"${PREFIX_GROUP}\""
    echo
    echo "# File extension for static LSM meta data files"
    echo "EXTENSION_DATA=\"${EXTENSION_DATA}\""
    echo
    echo "# File extension for static HTML pages"
    echo "EXTENSION_HTML=\"${EXTENSION_HTML}\""
    echo
    echo "# File extension for static RSS XML page"
    echo "EXTENSION_XML=\"${EXTENSION_XML}\""
    echo
    echo "# Filename character used for versioning DATA and HTML files"
    echo "VERSIONING=\"${VERSIONING}\""
    echo
    echo "# Ignored file names if package group directories. Must be surrounded by ; "
    echo "# (note all files in repository root directory are ignored automatically)"
    echo "FILE_IGNORE=\"${FILE_IGNORE}\""
    echo
    echo "# Ignored meta-data fields of packages when creating CSV files. Must be"
    echo "# surrounded by ; (note all fields not set to be displayed are"
    echo "# automatically excluded)"
    echo "CSV_EXCLUDE=\"${CSV_EXCLUDE}\""
    echo
    echo "# Include deleted packages in comparison chart"
    echo "COMPARE_DELETED=\"${COMPARE_DELETED}\""
    echo
    echo "# All generated files are also compared before being placed. These filters"
    echo "# are used to filter out generation time stamps to prevent updating"
    echo "# existing unchanged files and preserve their modification time stamps."
    echo "FILTER_META=\"${FILTER_META}\""
    echo "FILTER_LST=\"${FILTER_LST}\""
    echo "FILTER_TXT=\"${FILTER_TXT}\""
    echo "FILTER_HTML=\"${FILTER_HTML}\""
    echo "FILTER_CSS=\"${FILTER_CSS}\""
    echo "FILTER_XML=\"${FILTER_XML}\""
    echo
    echo "# External function shortcuts"
    echo "FUNCTION_DATELOG='$FUNCTION_DATELOG'"
    echo "FUNCTION_DATEMOD='$FUNCTION_DATEMOD'"
    echo "FUNCTION_DATESTAMP='$FUNCTION_DATESTAMP'"
    echo "FUNCTION_DATERSS='$FUNCTION_DATERSS'"
    echo "FUNCTION_DATEYEAR='$FUNCTION_DATEYEAR'"
    echo "FUNCTION_EPOCH_TO_DATE='$FUNCTION_EPOCH_TO_DATE'"
    echo
    echo "# Platform specific routines. Change these when you want to break everything."
    echo "FUNCTION_STAT='$FUNCTION_STAT'"
    echo "FUNCTION_CRC='$FUNCTION_CRC'"
    echo "FUNCTION_MD5='$FUNCTION_MD5'"
    echo "FUNCTION_SHA='$FUNCTION_SHA'"
    echo "FUNCTION_FILESIZE='$FUNCTION_FILESIZE'"
    echo "FUNCTION_BYTESIZE='$FUNCTION_BYTESIZE'"
    echo "FUNCTION_BUILDIDX='$FUNCTION_BUILDIDX'"
    echo "FUNCTION_BUILDCSV='$FUNCTION_BUILDCSV'"
    echo "FUNCTION_BUILDHTML='$FUNCTION_BUILDHTML'"
    echo "FUNCTION_CDISO='$FUNCTION_CDISO'"
    echo "FUNCTION_CDTITLE='$FUNCTION_CDTITLE'"
    echo "FUNCTION_ALTISO='$FUNCTION_ALTISO'"
    echo "FUNCTION_ALTTITLE='$FUNCTION_ALTTITLE'"
    echo "FUNCTION_EDIT='$FUNCTION_EDIT'"
    echo

    echo "# CD/DVD ISO image file settings"
    echo "FILE_CD_ISO=\"${FILE_CD_ISO}\""
    echo "FILE_CD_FLOPPY=\"${FILE_CD_FLOPPY}\""
    echo "FILE_CD_FILES=\"${FILE_CD_FILES}\""
    echo

    echo "# Alternate CD/DVD ISO image file settings"
    echo "FILE_ALT_ISO=\"${FILE_ALT_ISO}\""
    echo "FILE_ALT_FLOPPY=\"${FILE_ALT_FLOPPY}\""
    echo "FILE_ALT_FILES=\"${FILE_ALT_FILES}\""
    echo

    echo "# Maximum description width in index data files"
    echo "MAX_WIDTH='${MAX_WIDTH}'"
    echo

    echo "# Maximum items in rss feeds and initial new feed item count"
    echo "MAX_RSS='${MAX_RSS}'"
    echo "MAX_RSS_NEW='${MAX_RSS_NEW}'"
    echo

    echo "# Individual repository configuration information"

    i=0
    while [[ "${REPO_ID[$i]}" != "" ]] ; do
        echo "REPO_ID[$i]=\"${REPO_ID[$i]}\""
        [[ "${REPO_FAKED[$i]}" != "" ]] && echo "REPO_FAKED[$i]=\"${REPO_FAKED[$i]}\""
        [[ "${REPO_LOCKED[$i]}" != "" ]] && echo "REPO_LOCKED[$i]=\"${REPO_LOCKED[$i]}\""
        [[ "${REPO_TITLE[$i]}" != "" ]] && echo "REPO_TITLE[$i]=\"${REPO_TITLE[$i]}\""
        [[ "${REPO_PATH[$i]}" != "" ]] && echo "REPO_PATH[$i]=\"${REPO_PATH[$i]}\""
        [[ "${REPO_URL[$i]}" != "" ]] && echo "REPO_URL[$i]=\"${REPO_URL[$i]}\""
        [[ "${REPO_UPDATE[$i]}" != "" ]] && echo "REPO_UPDATE[$i]=\"${REPO_UPDATE[$i]}\""
        echo
        (( i++ ))
    done

    echo "# Put these groups first. (by ID, ';' separated)"
    echo "GROUP_ORDER=\"${GROUP_ORDER}\""
    echo
    i=0
    echo "# Group renaming. 'ID, Title, Description'"
    while [[ "${GROUP_ID[$i]}" != "" ]] ; do
        data="GROUP_DATA[$i]=\"${GROUP_ID[$i]}"
        if [[ "${GROUP_TITLE[$i]}" != "" ]] || [[ "${GROUP_DESCRIPTION[$i]}" != "" ]] ; then
            data="${data},${GROUP_TITLE[$i]}"
        fi
         if [[ "${GROUP_DESCRIPTION[$i]}" != "" ]] ; then
            data="${data},${GROUP_DESCRIPTION[$i]}"
        fi
        data="${data}\""
        echo "${data}"
        (( i++ ))
    done
    echo


    echo "# Package configuration information (fields not in this list are hidden)"
    echo "# LSM-Metadata-ID,ITEM:HANDLER,Displayed description "
    i=0
    while [[ "${LSM_ID[$i]}" != "" ]] ; do
        data="META_DATA[$i]=\"${LSM_ID[$i]}"
        if [[ "${LSM_TITLE[$i]}" != "" ]] || [[ "${LSM_HANDLER[$i]}" != "" ]] ; then
            data="${data},${LSM_HANDLER[$i]}"
            [[ "${LSM_TITLE[$i]}" != "" ]] && data="${data},${LSM_TITLE[$i]}"
        fi
        data="${data}\""
        echo "${data}"
        (( i++ ))
    done
    echo

    echo "# Website URL alaises. If a URL resides under the alias, most of the link text"
    echo "# will be replaced with it's title. Never include WWW., it is always assumed"
    echo "# to be the default server for http urls and is stripped prior to matching."
    echo "# Including it will cause the alias to never match a URL."
    i=0
    while [[ "${SITE_ALIAS[$i]}" != "" ]] ; do
        echo "SITE_ALIAS[$i]=\"${SITE_ALIAS[$i]}\""
        echo "SITE_TITLE[$i]=\"${SITE_TITLE[$i]}\""
        (( i++ ))
    done
    echo
}

# Write the configuration file
function config_write
{
    show_config >"$CONFIG"
}

# Perform fail-safe system rm
function system_rm
{
    while [[ "$1" != "" ]] ; do
        if [[ -f "$1" ]] ; then
            rm "$1" || show_error 2 "Unable to remove file \`${1}'"
        elif [[ -L "$1" ]] ; then
            rm "$1" || show_error 2 "Unable to remove link \`${1}'"
        elif [[ -d "$1" ]] ; then
            rm -rf "$1" || show_error 2 "Unable to remove directory \`${1}'"
        fi
        shift
    done
}

# Perform fail-safe system touch
function system_touch
{
    while [[ "$1" != "" ]] ; do
        touch "$1" || show_error 2 "Unable to create file \`${1}'"
        shift
    done
}

# Perform fail-safe system mkdir
function system_mkdir
{
    while [[ "$1" != "" ]] ; do
        if [[ ! -d "$1" ]] ; then
            mkdir "$1" || show_error 2 "Unable to make directory \`${1}'"
        fi
        shift
    done
}

# Perform fail-safe system ln -s
function system_ln
{
    local src="$1"
    shift
    while [[ "$1" != "" ]] ; do
        ln -s "$src" "$1" || show_error 2 "Unable to create link \`${1}' to \`${src}'"
        shift
    done
}

# Perform fail-safe system mv
function system_mv
{
    while [[ "$1" != "" ]] ; do
        mv "$1" "$2" || show_error 2 "Unable to move \`${1}' to \`${2}'"
        shift
        shift
    done
}

# Perform fail-safe system cp
function system_cp
{
    while [[ "$1" != "" ]] ; do
        cp -a "$1" "$2" || show_error 2 "Unable to copy \`${1}' to \`${2}'"
        shift
        shift
    done
}

# Perform fail-safe system cd
function system_cd
{
    while [[ "$1" != "" ]] ; do
        cd "$1" || show_error 1 "Unable to change directory \`${1}'"
        shift
    done
}

# Perform filtered file comparison before replacing file
# Note: returns 0 if replaced, 1 if not needed and was just removed.
function place_file
{
    if [[ ! -e "$2" ]] ; then
        local doit=true
    elif [[ "${2##*.}" == "gz" ]] || [[ "${2##*.}" == "zip" ]] ; then
        local doit=true
    else
        local doit=false
        local filter=$(caseUpper "FILTER_${2##*.}")
        local meta=$(caseUpper "FILTER_${EXTENSION_DATA}")
        local inrepo="${2:$(( ${#REPOSITORY_PATH} + 1 ))}"
        if [[ "$filter" == "$meta" ]] && [[ "${inrepo:0:$(( ${#PATH_DATA} + 1 ))}" == "${PATH_DATA}/" ]] ; then
            filter="FILTER_META"
        fi
        filter="${!filter}"
        if [[ "$filter" == "" ]] ; then
            diff -a "$1" "$2" >/dev/null 2>&1 || local doit=true
        else
            system_rm "${PATH_TEMP}/tempdiff1-${1##*/}" "${PATH_TEMP}/tempdiff2-${2##*/}"
            cat "$1" | grep -a  -iv "$filter" >"${PATH_TEMP}/tempdiff1-${1##*/}"
            cat "$2" | grep -a  -iv "$filter" >"${PATH_TEMP}/tempdiff2-${2##*/}"
            diff -a "${PATH_TEMP}/tempdiff1-${1##*/}" "${PATH_TEMP}/tempdiff2-${2##*/}" >/dev/null 2>&1 || local doit=true
#            diff -a "${PATH_TEMP}/tempdiff1-${1##*/}" "${PATH_TEMP}/tempdiff2-${2##*/}" 2>&1
            system_rm "${PATH_TEMP}/tempdiff1-${1##*/}" "${PATH_TEMP}/tempdiff2-${2##*/}"
        fi
    fi

    if [[ $doit == true ]] ; then
        system_mv "$1" "$2"
        return 0
    else
        [[ "$HUSH" == "" ]] && message "No significant changes to \`${2:$(( ${#REPOSITORY_PATH} + 1 ))}' (preserved)."
        system_rm "$1"
        return 1
    fi
}

# Select a repository by identity
function repository_select
{
    [[ "$1" == "" ]] && show_error 1 "No repository specified"
    local search=$(caseLower "$1")
    local i=0
    while [[ "${REPO_ID[$i]}" != "" ]] && [[ "${REPO_ID[$i]}" != "$search" ]]; do
        (( i++ ))
    done
    [[ "${REPO_ID[$i]}" == "" ]] && show_error 1 "Repository \`$1' not found"
    [[ "${REPO_PATH[$i]}" == "" ]] && show_error 1 "Repository \`$1' path not specified"

    REPOSITORY_SERVER="$SERVERNAME"
    REPOSITORY_ID="$search"
    REPOSITORY_NAME="$REPOSITORY_ID"
    REPOSITORY_TITLE="${REPO_TITLE[$i]}"
    REPOSITORY_PATH="${REPO_PATH[$i]}"
    REPOSITORY_URL="${REPO_URL[$i]}"
    REPOSITORY_LOCKED="${REPO_LOCKED[$i]}"
    REPOSITORY_FAKED="${REPO_FAKED[$i]}"
    REPOSITORY_UPDATE="${REPO_UPDATE[$i]}"
    [[ "${REPOSITORY_UPDATE}" == "" ]] && REPOSITORY_UPDATE="${PATH_UPDATES}"
    REPOSITORY_NUMBER="$i"

    [[ "$REPOSITORY_TITLE" == "" ]] && REPOSITORY_TITLE="$REPOSITORY_NAME"
    [[ "$REPOSITORY_URL" == "" ]] && REPOSITORY_URL="$REPOSITORY_PATH"
    [[ "$REPOSITORY_LOCKED" == "" ]] && REPOSITORY_LOCKED=no
    [[ "$REPOSITORY_FAKED" == "" ]] && REPOSITORY_FAKED=no

    [[ ! -e "$REPOSITORY_PATH" ]] && error 1 "Repository \`$@' path not found"
    system_cd "$REPOSITORY_PATH"

    [[ "$2" == "QUIET" ]] && return 0

    if [[ "$REPOSITORY_NAME" != "$REPOSITORY_TITLE" ]] ; then
        message "Repository \`$REPOSITORY_TITLE' ($REPOSITORY_NAME) selected."
    else
        message "Repository \`$REPOSITORY_NAME' selected."
    fi
    (( INDENT += 2 ))
    message "Path: $REPOSITORY_PATH"
    message "URL: $REPOSITORY_URL"
#    message "Locked: $REPOSITORY_LOCKED"
#    message "Faked: $REPOSITORY_FAKED"
    message
}

# Display a list of the available repositories
function repository_list
{
    local i=0
    local s

    while [[ "${REPO_ID[$i]}" != "" ]] && [[ "${REPO_ID[$i]}" != "$search" ]]; do
        [[ "${REPO_ID[$i]}" == "$REPOSITORY_ID" ]] && s=" (CURRENT)" || s=" "
        [[ "${REPO_ID[$i]}" == "$REPOSITORY" ]] && s="${s} (DEFAULT)"
        [[ "${REPO_TITLE[$i]}" != "" ]] && s=" Title: \`${REPO_TITLE[$i]}'${s}"
        message "Repository \`${REPO_ID[$i]}'${s}"
        (( INDENT += 2 ))
        message "Path: ${REPO_PATH[$i]}"
		if [[ "${REPO_LOCKED[$i]}" == "" ]] && [[ "${REPO_FAKED[$i]}" == "" ]] ; then 
	        [[ "${REPO_UPDATE[$i]}" != "" ]] && message "Updates: ${REPO_URL[$i]}" || message "Updates: (default)"
		fi
        [[ "${REPO_URL[$i]}" != "" ]] && message "URL: ${REPO_URL[$i]}"
        [[ "${REPO_LOCKED[$i]}" != "" ]] && message "Locked: ${REPO_LOCKED[$i]}"
        [[ "${REPO_FAKED[$i]}" != "" ]] && message "Faked: ${REPO_FAKED[$i]}"
        message
        (( INDENT -= 2 ))
        (( i++ ))
    done
}

# Checks that a repository is neither locked or faked, returns 1 if either
# condition is met.
function repository_protected
{
    if [[ "$REPOSITORY_FAKED" == yes ]] || [[ "$REPOSITORY_FAKED" == true ]]; then
        log "Repository is faked. Process skipped."
        return 0
    elif [[ "$REPOSITORY_LOCKED" == yes ]] || [[ "$REPOSITORY_LOCKED" == true ]] ; then
        log "Repository is locked. Process skipped."
        return 0
    fi
    return 1
}

# Converts a string to lower case
function caseLower
{
    echo "$@" | tr "[:upper:]" "[:lower:]"
}

# Converts a string to upper case
function caseUpper
{
    echo "$@" | tr "[:lower:]" "[:upper:]"
}

# Coverts words to capitalized
function caseCapital
{
    local str="$1"
    local ret
    local word

    while [[ "$str" != "" ]] ; do
        word="${str%% *}"
        [[ "$word" == "" ]] && word=" "
        if [[ "$str" == "$word" ]] ; then
            str=""
        else
            str="${str:$(( ${#word} + 1 ))}"
        fi
        ret="$ret $(caseUpper ${word:0:1})$(caseLower ${word:1})"
    done
    echo "$ret"
}

# Converts all to uppercase and removes non-alphanum characters
function stripUpper
{
    echo "$@" | tr -cd '[:alnum:]' | tr '[:lower:]' '[:upper:]'
}

# Converts all to lowercase and removes non-alphanum characters
function stripLower
{
    echo "$@" | tr -cd '[:alnum:]' | tr '[:upper:]' '[:lower:]'
}

# Removes control characters
function noControl
{
    echo "$@" | tr -d "[:cntrl:]"
}

# Converts tab characters to single space
function noTabs
{
    echo "$@" | tr "\t" " "
}

# Return newest file in directory
function file_latest
{
    ls -1td "$1/"* | head -n 1
}

# Return main package file in any group
function find_package
{
    local group

    for group in "$REPOSITORY_PATH"/* ; do
        if [[ "${group}" == "$REPOSITORY_PATH/*" ]] ; then
            :; # No Directories, Hunh?
        elif [[ ! -d "${group}" ]] ; then
            :; # Ignore non-directories in repository root, like index and things
        elif [[ "${group}" == "$REPOSITORY_PATH/$PATH_DATA" ]] ; then
            :; # Ignore DATA path
        elif [[ "${group}" == "$REPOSITORY_PATH/$PATH_HTML" ]] ; then
            :; # Ignore HTML path
        else
            local search=$(ls -1td "${group}/$1".* 2>/dev/null | head -n 1)
            if [[ "$search" != "" ]] ; then
                echo "$search"
                return 0
            fi;
        fi
    done
    return 1
}

# Attempt to expand the path of a given filename
function path_expand
{
    local hold="$PWD"
    local new="$1"
    [[ "${new:0:1}" == "/" ]] && {
        echo "${new}"
        return 1
    }
    [[ "${new%/*}" != "${new}" ]] && {
        cd "${new%/*}" || {
            cd "$hold"
            echo "${PWD}/${new}"
            return 1
        }
    }
    echo "${PWD}/${new##*/}"
    cd "$hold"
    return 0
}

# Returns Target of link, or file/directory name if not a link
function link_target
{
    if [[ -L "${1}" ]] ; then
        local real=$(ls -ald "${1}")
        real="${real##* -> }"
        # if [[ "${real:0:1}" == "a" ]] ; then
        #     echo "${real}"
        # else
            [[ "${1%/*}" != "$1" ]] && echo "${1%/*}/${real}" || echo "${real}"
        # fi
        return 0
    else
        echo "${1}"
        return 1
    fi;
}

# Returns a list of packages for a group
function package_list
{
    [[ ! -d "${REPOSITORY_PATH}/$1" ]] && show_error 1 "Invalid group \`$1'"
    local hold="$PWD"
    local i
    system_cd "${REPOSITORY_PATH}/$1"
    for i in * ; do
        [[ -e "$i" ]] && [[ ! -d "$i" ]] && [[ "${FILE_IGNORE//;${i};/}" == "$FILE_IGNORE" ]] && echo "${i}"
    done
    cd "$hold"
    return 0
}

# Returns a list of packages for a group sorted by title
function sorted_package_list
{
    local i
    local x
    local f

    for i in $(package_list "$1") ; do
        f="${REPOSITORY_PATH}/${PATH_DATA}/${i%.*}.${EXTENSION_DATA}"
        if [[ -e "${f}" ]] ; then
            x=$(grep -a  -i "^TITLE:" "${f}" | cut -d ':' -f 2- | tr -d "[:cntrl:]" || echo )
            x=$(caseLower ${x})
            [[ "${x:0:4}" == "the " ]] && x="${x:4}"
            [[ "${x:0:2}" == "a " ]] && x="${x:2}"
            [[ "${x:0:3}" == "an " ]] && x="${x:3}"
            [[ "$x" == "" ]] && x="${i%.*}"
            echo "${x//,/},$i"
        fi;
    done | sort | cut -d ',' -f 2
}

function package_repo_list
{
    local i
    local p

    for i in $(cat "${PATH_TEMP}/packagelist.tmp" | grep -a  -i "^${1}/") ; do
        p="${i##*/}"
        echo "${p%.*}"
    done
}

# Returns a list of packages for a group sorted by title
function sorted_package_repo_list
{
    local list="$COMPARE_LIST"
    local i
    local f
    local x
    local this
    local pkgs

    while [[ "$list" != "" ]] ; do
        this="${list%%,*}"
        list="${list:$(( ${#this} + 1))}"
        # if [[ -d "${REPO_PATH[$this]}/$1" ]] ; then
            repository_select "${REPO_ID[$this]}" QUIET
            for i in $(package_repo_list "$1") ; do
                [[ "${pkgs//;$i;/}" != "${pkgs}" ]] && continue
                f="${REPOSITORY_PATH}/${PATH_DATA}/${i%.*}.${EXTENSION_DATA}"
                if [[ -e "${f}" ]] ; then
                    pkgs="${pkgs};${i};"
                    x=$(grep -a  -i "^TITLE:" "${f}" | cut -d ':' -f 2- | tr -d "[:cntrl:]" || echo )
                    x=$(caseLower ${x})
                    [[ "${x:0:4}" == "the " ]] && x="${x:4}"
                    [[ "${x:0:2}" == "a " ]] && x="${x:2}"
                    [[ "${x:0:3}" == "an " ]] && x="${x:3}"
                    [[ "$x" == "" ]] && x="${i%.*}"
                    echo "${x//,/},$i"
                fi;
            done
        # fi
    done | sort -u | cut -d ',' -f 2
    repository_select "${REPOSITORY}" QUIET
}

# Adds protocol to URL (ie, http:// or ftp:// ) if missing
function url_fix
{
    local u="$@"
    local p="${u%%://*}"
    [[ "$p" == "$u" ]] && p=
    if [[ "$p" != "" ]] ; then
        echo "$u"
    else
        if [[ "${u:0:3}" == "ftp" ]] ; then
            p="ftp"
        else
            p="http"
        fi
        echo "$p://$u"
    fi
}

# Guesses at changing a link text based on aliased sites
function url_title
{
    local ret
    local i=0
    local url="$1"
    [[ "${url:$(( ${#url} - 1 ))}" == "/" ]] && [[ ${#url} -gt 1 ]] && url="${url:0:$(( ${#url} - 1 ))}"
    while [[ "${SITE_ALIAS[$i]}" != "" ]] && [[ "$ret" == "" ]] ; do
        if [[ "${url:0:${#SITE_ALIAS[$i]}}" == "${SITE_ALIAS[$i]}" ]] ; then
            ret="${SITE_TITLE[$i]}"
            ret="${ret/\<URL\>/${url:${#SITE_ALIAS[$i]}}}"
        fi;
        (( i++ ))
    done

    [[ "$ret" == "" ]] && echo "$url" || echo "$ret"
}

function path_relate
{
        local srv="$PWD"
        local url="$1"
        local count=0
        local flag=true
        local fixed
        local udir
        local sdir

        while [[ "$srv" != "" ]] && [[ "$url" != "" ]] && [[ "$flag" == "true" ]]; do
            udir="${url%%/*}"
            sdir="${srv%%/*}"
            if [[ "$udir" != "$sdir" ]] ; then
                flag=false
            else
                flag=true
                url="${url:$(( ${#udir} + 1 ))}"
                srv="${srv:$(( ${#sdir} + 1 ))}"
                [[ "$fixed" == "" ]] && fixed="/${url}"
                (( count++ ))
            fi
        done
        if [[ "$count" != "0" ]] ; then
            count=0
            while [[ "$srv" != "" ]] ; do
                sdir="${srv%%/*}"
                srv="${srv:$(( ${#sdir} + 1 ))}"
                url="../${url}"
                (( count++ ))
            done
            if [[ $count -eq 0 ]] ; then
                URL_TITLE="${REPOSITORY_TITLE} - ${DATAFIELD_TITLE}"
            else
                URL_TITLE=$(url_title "$check")
            fi;
            if [[ ${#fixed} -lt ${#url} ]] ; then
                # message "URL \`$1' on same domain made absolute \`$fixed'"
                echo "$fixed"
            else
                # message "URL \`$1' made relative as \`$url'"
                echo "$url"
            fi;
            return 0
        fi
        echo "$1"
}

# If URL is on same server than make it relative
function url_relate
{
    URL_RELATIVE="$@"
    URL_TITLE="$@"

    local url="${@}"
    local srv="${REPOSITORY_URL}/${PATH_HTML}"
    local uprot
    local sprot

    if [[ "${url:0:1}" == "/" ]] ; then
        uprot='/'
        url="${url:1}"
    else
        uprot="${url%%://*}"
        url="${url#*://}"
        [[ "${url:0:4}" == "www." ]] && url="${url:4}"
    fi
    [[ "$uprot" == "https" ]] && uprot="http"
    if [[ "${srv:0:1}" == "/" ]] ; then
        sprot='/'
        srv="${srv:1}"
    else
        sprot="${srv%%://*}"
        srv="${srv#*://}"
        [[ "${srv:0:4}" == "www." ]] && srv="${srv:4}"
    fi
    [[ "$sprot" == "https" ]] && sprot="http"

    local check="${uprot}://${url}"

    if [[ "$sprot" == "$uprot" ]] ; then
        local count=0
        local flag=true
        local fixed
        local udir
        local sdir

        while [[ "$srv" != "" ]] && [[ "$url" != "" ]] && [[ "$flag" == "true" ]]; do
            udir="${url%%/*}"
            sdir="${srv%%/*}"
            if [[ "$udir" != "$sdir" ]] ; then
                flag=false
            else
                flag=true
                url="${url:$(( ${#udir} + 1 ))}"
                srv="${srv:$(( ${#sdir} + 1 ))}"
                [[ "$fixed" == "" ]] && fixed="/${url}"
                (( count++ ))
            fi
        done
        if [[ "$count" != "0" ]] ; then
            count=0
            while [[ "$srv" != "" ]] ; do
                sdir="${srv%%/*}"
                srv="${srv:$(( ${#sdir} + 1 ))}"
                url="../${url}"
                (( count++ ))
            done
            if [[ $count -eq 0 ]] ; then
                URL_TITLE="${REPOSITORY_TITLE} - ${DATAFIELD_TITLE}"
            else
                URL_TITLE=$(url_title "$check")
            fi;
            if [[ ${#fixed} -lt ${#url} ]] ; then
                # message "URL \`$1' on same domain made absolute \`$fixed'"
                echo "$fixed"
                URL_RELATIVE="$fixed"
            else
                # message "URL \`$1' made relative as \`$url'"
                echo "$url"
                URL_RELATIVE="$url"
            fi;
            return 0
        fi
    fi
    URL_TITLE=$(url_title "$check")
    echo "$@"
}

# Convert string characters that conflict with HTML
function sanitize_html
{
    local safe="${@//&/&amp;}"
    safe="${safe//</&lt;}"
    safe="${safe//>/&gt;}"
    echo "${safe}"
}

# Convert string characters that conflict index data files
function sanitize_index
{
    local safe=$(echo "${@}" | tr -d '\t\r\n\a\b\f\v')
    echo "${safe:0:${MAX_WIDTH}}"
}

# General note that something needs re-done.
function mark_for_redo
{
    local class=$(caseUpper "${1}")
    local classid="REBUILD_${class}"
    shift
    local group=$(caseLower "${1%%/*}")
    [[ "${!classid//:${group};/}" != "${!classid}" ]] && return 0
    read -r $classid<<<$(echo "${!classid}:$group;")
    (( INDENT -= 2 ))
    message "Mark \`${group}' for \`${classid#*_}' rebuild"
    (( INDENT += 2 ))
}

# Simple test for group html and index file exists
function verify_group_index_exists
{
    local flag=false
    [[ ! -e "${REPOSITORY_PATH}/$1/index.lst" ]] && flag=true
    [[ ! -e "${REPOSITORY_PATH}/$1/index.gz" ]] && flag=true
    [[ ! -e "${REPOSITORY_PATH}/${PATH_HTML}/${PREFIX_GROUP}$1.${EXTENSION_HTML}" ]] && flag=true
    [[ "$flag" == "true" ]] && mark_for_redo INDEX "${1}"
}

# Verify repository filessystem and directory integrity
function update_filesystem
{
    message "Verifying repository structure and integrity"
    repository_protected && return 0

    (( INDENT += 2 ))

    local line
    local name
    local dir
    local fname
    local fext
    local fstamp
    local lname
    local lstamp
    local hold
    local rebuild
    local data
    local dstamp
    local subdir

    for line in $(find . | cut -d '/' -f 2- | grep -a  -iv "^${PATH_DATA}\|^${PATH_HTML}" | sort -r) ; do

        name="${line##*/}"
        dir="${line%/*}"

        # echo $dir -- $name
        [[ "$dir" == "$line" ]] && dir= || dir="${dir}/"
        if [[ "${name:0:1}" == "." ]] ; then
            :;
            # Ignore . Files and directories
        elif [[ -L "${line}" ]] ; then
            # Check if link is valid
            if [[ ! -e "$line" ]] ; then
                log "invalid link \`${line}' remove"
                system_rm "$line"
                mark_for_redo INDEX "${line%%/*}"
                continue
            fi
            # Check case of link
            if [[ "$name" != "$(caseLower $name)" ]] ; then
                log "lowercase link \`${line}' to \`${dir}$(caseLower $name)'"
                system_mv "${REPOSITORY_PATH}/${line}" "${REPOSITORY_PATH}/${dir}$(caseLower $name)"
                mark_for_redo INDEX "${line%%/*}"
            fi
        elif [[ -d "${line}" ]] ; then
            # Adjust directory permission
            chmod 755 "${REPOSITORY_PATH}/${line}" || show_error 1 "Unable to set permissions for file \`${line}'"
            # Check case of directory name
            if [[ "$name" != "$(caseLower $name)" ]] ; then
                name="$(caseLower $name)"
                log "lowercase path \`${line}' to \`${dir}${name}'"
                system_mv "${REPOSITORY_PATH}/${line}" "${REPOSITORY_PATH}/${dir}${name}"
                [[ "$dir" != "" ]] && mark_for_redo DATA "$name"
                mark_for_redo INDEX "${line%%/*}"
            fi

            # Test if directory is a versioned package
            if [[ "$dir" != "" ]] ; then
                fname=$(file_latest "${line}")
                fext="${fname##*.}"
                fstamp=$(external STAT "${fname}")
                lname="${fname%/*}.${fext}"
                [[ -L "$lname" ]] && lstamp=$(external STAT "${lname}") || lstamp=0
                # If link is not newer than latest package
                if [[ $lstamp -lt $fstamp ]] ; then
                    # If link does not exist
                    hold="$PWD"
                    system_cd "${lname%/*}"
                    lname="${lname##*/}"
                    fname="${lname%.*}/${fname##*/}"
                    if [[ $lstamp -eq 0 ]] ; then
                        log "create missing link \`${line%/*}/${lname}' for \`${line%/*}/${fname}'"
                        system_ln "${fname}" "${lname}"
                    else
                        log "recreate link \`${line%/*}/${lname}' for \`${line%/*}/${fname}'"
                        system_rm "${lname}"
                        system_ln "${fname}" "${lname}"
                    fi;
                    cd "$hold"
                    mark_for_redo DATA "$name"
                    mark_for_redo INDEX "${line%%/*}"
                fi
                [[ "$FORCE" == "yes" ]] && mark_for_redo DATA "$name"
            fi
            [[ "$FORCE" == "yes" ]] && mark_for_redo INDEX "${line%%/*}"
        elif [[ -f "${line}" ]] && [[ "${FILE_IGNORE//;${name};/}" == "$FILE_IGNORE" ]] ; then
            # Adjust file permissions
            chmod 744 "${REPOSITORY_PATH}/${line}" || show_error 1 "unable to set permissions for file \`${line}'"
            # Check case of file name
            [[ "$FORCE" == "yes" ]] && rebuild=yes || rebuild=no
            if [[ "$name" != "$(caseLower $name)" ]] ; then
                name="$(caseLower $name)"
                log "lowercase file \`${line}' to \`${dir}${name}'"
                system_mv "${REPOSITORY_PATH}/${line}" "${REPOSITORY_PATH}/${dir}${name}"
                rebuild=yes
            fi

            # If it is in a version in sub-directory
            if [[ "$dir" != "" ]]  ; then
                # Check that data file exists and is newer
                data="${line#*/}"
                [[ "${data//\//$VERSIONING}" != "${data}" ]] && data="${data//\//$VERSIONING}"

                data=$(safe_filename "${data%.*}")
                data="${data}.${EXTENSION_DATA}"

                if [[ ! -f "$REPOSITORY_PATH/$PATH_DATA/${data}" ]] ; then
                    rebuild=yes
                elif [[ ! -f "$REPOSITORY_PATH/$PATH_HTML/${data%.*}.${EXTENSION_HTML}" ]] ; then
                    rebuild=yes
                else
                    fstamp=$(external STAT "${line}")
                    dstamp=$(external STAT "$REPOSITORY_PATH/$PATH_DATA/${data}")
                    [[ $dstamp -lt $fstamp ]] && rebuild=yes
                fi
            else
                # Never rebuild if file is in top repository directory
                rebuild=no
            fi

            if [[ "$rebuild" == "yes" ]] ; then
                subdir="${dir#*/}"
                [[ "$subdir" == "" ]] && mark_for_redo DATA "$name" || mark_for_redo DATA "$subdir"
                mark_for_redo INDEX "${line%%/*}"
            fi
        fi
    done

    for_all_groups verify_group_index_exists

    (( INDENT -= 2 ))
}

# Clears all variables related to a package
function clear_field_data
{
    local item
    while [[ "$DATAFIELDS" != "" ]] ; do
        item="${DATAFIELDS%%;*}"
        DATAFIELDS="${DATAFIELDS#*;}"
        [[ "$DATAFIELDS" == "$item" ]] && DATAFIELDS=""
        item="DATAFIELD_${item:1}"
        unset $item
    done

    DATAPACKAGE_ID=
    DATAPACKAGE_DATAFILE=
    DATAPACKAGE_NAME=
    DATAPACKAGE_URL=
    DATAPACKAGE_FILEPATH=
    DATAPACKAGE_FILENAME=
    DATAPACKAGE_FILETARGET=
    DATAPACKAGE_FILEURL=
    DATAPACKAGE_FILESIZE=
    DATAPACKAGE_BYTESIZE=
    DATAPACKAGE_VERSION=
    DATAPACKAGE_GUID=
    DATAPACKAGE_TEXT=
    DATAPACKAGE_CONTENT=
}

# Creates and sets a specific variable field for a package
function set_field_data
{
    local field=$(stripUpper "$1")
    if [[ "${DATAFIELDS//:${field};/}" == "${DATAFIELDS}" ]] ; then
        DATAFIELDS="${DATAFIELDS}:$field;"
    fi
    field="DATAFIELD_${field}"
    read -r $field<<<"$2"
}

function relocate () {
    local p="${1}"
    log "internal error: package dir \"${p}\" not found, PWD: \"${PWD}\""
    p=$(ls -a1d *"/${p}/" 2>/dev/null || echo "internal_error")
    p="${p%/*}"
    log "reset to \"${p}\""
    echo "${p}"
}

# Returns file name based versions ID in a subdirectory.
function get_version_ids
{
    local i
    local t
    local p="${1}"

    # log "+++ ${PWD} - \"${p}\""
    if [[ ! -d "${p}" ]] ; then
        p=$(relocate "${p}")
    fi
    if [[ "${p:0:1}" != '/' ]] ; then
        p="${PWD}/${p}"
        # log "+++ \"${p}\""
    fi
    for i in $(ls -1dt "${p}/"*) ; do
        t="${i##*/}"
        echo "${t%%.*}"
        # log "+++ versions: \"${i}\" as \"${t%%.*}\""
    done
    # log '---'
}

# Populates all variables related to a given package (and version)
# if $2 == dumb, then fields are populated, but no processing is performed
function populate_item_data
{
    [[ ! -f "$1" ]] && show_error 1 "missing data for \`${1}'"

    clear_field_data

    local line
    local field

    while IFS="" read -r line ; do
        if [[ "${line:0:1}" != " " ]] ; then
            field="${line%%:*}"
            if [[ "$field" != "$line" ]] ; then
                line="${line:$(( ${#field} + 1))}"
                line=$(noControl "$line")
                while [[ "${line:0:1}" == " " ]] ; do
                    line="${line:1}"
                done
                set_field_data "${field}" "${line}"
                field=$(caseUpper $field)
            fi;
        fi
    done<"${1}"

    [[ "$(caseUpper $DATAFIELD_TITLE)" == "$DATAFIELD_TITLE" ]] && DATAFIELD_TITLE=$(caseLower "$DATAFIELD_TITLE")
    [[ "${DATAFIELD_VERSION// /}" == "" ]] && set_field_data VERSION "unknown"

    [[ "$2" == "dumb" ]] && return 0

    line="${1##*/}"
    line="${line%.*}"
    DATAPACKAGE_ID="${line%%.*}"
    DATAPACKAGE_DATAFILE="${1}"
    DATAPACKAGE_NAME="$DATAPACKAGE_ID"
    DATAPACKAGE_URL="${DATAPACKAGE_ID}.${EXTENSION_HTML}"
    # log "+++ ID: $DATAPACKAGE_ID"

    local this=$(find_package "$DATAPACKAGE_ID")
    # log "+++ WHERE: $this"
    DATAPACKAGE_FILENAME="$this"
    DATAPACKAGE_FILETARGET="${this}"
    DATAPACKAGE_VERSIONS=
    local latest=$(link_target "$this")
    local fsize=$(external FILESIZE "$latest")

    # log "+++ NEW: $latest"


    DATAPACKAGE_BYTESIZE=$(external BYTESIZE "$latest")
    DATAPACKAGE_LINK="${DATAPACKAGE_ID}"

    if [[ -d "${this%.*}" ]] ; then
        local lver="${latest##*/}"
        DATAPACKAGE_VERSIONS="$( get_version_ids ${latest%/*} )"
        lver="${lver%%.*}"
        local tver="${line##*.}"
        if [[ "$lver" != "$tver" ]] ; then
            DATAPACKAGE_FILENAME="${this%.*}/${tver}.${this##*.}"
            local fsize=$(external FILESIZE "$DATAPACKAGE_FILENAME")
            DATAPACKAGE_FILETARGET="${DATAPACKAGE_FILENAME}"
            DATAPACKAGE_LINK="${DATAPACKAGE_LINK}${VERSIONING}${lver%.*}"
        else
            DATAPACKAGE_FILETARGET="${latest}"
        fi;
    fi;

    DATAPACKAGE_FILESIZE=$fsize
    DATAPACKAGE_FILEPATH="${DATAPACKAGE_FILENAME%/*}"
    DATAPACKAGE_FILENAME="${DATAPACKAGE_FILENAME:${#REPOSITORY_PATH}}"
    DATAPACKAGE_FILEURL="$(url_relate ${REPOSITORY_URL}${DATAPACKAGE_FILENAME})"
    DATAPACKAGE_LINK="${REPOSITORY_URL}/${PATH_HTML}/${DATAPACKAGE_LINK}.${EXTENSION_HTML}"
    DATAPACKAGE_FILENAME="${DATAPACKAGE_FILENAME##*/}"
    DATAPACKAGE_GUID=$(safe_filename "${DATAPACKAGE_FILENAME%.*}-${DATAFIELD_VERSION}")
    [[ "$DATAFIELD_SUMMARY" != "" ]] && DATAPACKAGE_TEXT="$DATAFIELD_SUMMARY" || DATAPACKAGE_TEXT="$DATAFIELD_DESCRIPTION"
}

# Populates all variables related to a group.
function populate_group_data_common
{
    DATAGROUP_ID="$1"
    DATAGROUP_NAME="$1"
    DATAGROUP_TITLE="$1"
    DATAGROUP_DESCRIPTION=""
    local i=0
    while [[ "${GROUP_ID[$i]}" != "" ]] ; do
        if [[ "${GROUP_ID[$i]}" == "$1" ]] ; then
            [[ "${GROUP_TITLE[$i]}" != "" ]] && DATAGROUP_TITLE="${GROUP_TITLE[$i]}"
            DATAGROUP_DESCRIPTION="${GROUP_DESCRIPTION[$i]}"
        fi
        (( i++ ))
    done
    DATAGROUP_URL="${PREFIX_GROUP}${1}.${EXTENSION_HTML}"
}

function populate_group_data
{
    populate_group_data_common "$1"
    DATAGROUP_COUNT=$(package_list "${1}" | wc -l | tr -d "\t[:cntrl:] ")
}

function populate_group_data_from_list
{
    populate_group_data_common "$1"
    DATAGROUP_COUNT=$(grep -a  -i "^${1}/" "${PATH_TEMP}/packagelist.tmp" | wc -l | tr -d "\t[:cntrl:] ")
}

function clear_group_data
{
    DATAGROUP_ID=""
    DATAGROUP_NAME=""
    DATAGROUP_TITLE=""
    DATAGROUP_URL=""
    DATAGROUP_COUNT=0
}

# Returns a section from the template file
function getTemplate
{
   cat "$TEMPLATE" | grep -a  -A 10000 -i "^# $1\( \|\$\)" | grep -a  -iv "^# $1\( \|\$\)" | grep -a  -m 1 -B 10000 "^#" | grep -a  -iv "^#" || echo
}

# Invoked by the template to list the META_DATA of a package
function list_package
{
    process_html LIST "PACKAGE.HEADER"
    local i=0
    local this
    local count_vers

    while [[ "${LSM_ID[$i]}" != "" ]] ; do
        this="DATAFIELD_$(stripUpper ${LSM_ID[$i]})"
        this="${!this}"
		x="${this//[[:cntrl:]]}"
		x="${x// }"
		x="${x//-}"
        [[ "$x" == "" ]] && this=""
        [[ "$x" == "?" ]] && this=""
        [[ "${LSM_HANDLER[$i]}" == "DOWNLOAD" ]] && this="$(url_relate $DATAPACKAGE_FILENAME)"
        if [[ "${LSM_HANDLER[$i]}" == "VERSIONS" ]] ; then
            count_vers=$(echo "${DATAPACKAGE_VERSIONS}" | wc -l | tr -d "\t[:cntrl:] ")
            if [[ "$count_vers" == "1" ]] && [[ "${DATAPACKAGE_VERSIONS}" != "" ]] ; then
                message "Only one version of \`${DATAPACKAGE_ID}'"
                this=""
            else
                this="${DATAPACKAGE_VERSIONS}"
            fi
        fi
        if [[ "$this" != "" ]] ; then
            DATAITEM_ID="${LSM_ID[$i]}"
            DATAITEM_NAME="$DATAITEM_ID"
            DATAITEM_TITLE="$DATAITEM_ID"
            DATAITEM_TEXT="$DATAITEM_TITLE"
            DATAITEM_TARGET="_self"
            [[ "${LSM_TITLE[$i]}" != "" ]] && DATAITEM_TITLE="${LSM_TITLE[$i]}"
            DATAITEM_VALUE="${this}"
            if [[ "${LSM_HANDLER[$i]}" != "" ]] ; then
                DATAITEM_VALUE=$(process_html ITEM "${LSM_HANDLER[$i]}")
            else
                DATAITEM_VALUE=$(process_html ITEM DEFAULT)
            fi
            process_html LIST "PACKAGE.ITEM"
        fi;
        (( i++ ))
    done
    process_html LIST "PACKAGE.FOOTER"
}

# Invoked by template to list all versions of the current package.
function list_versions
{
    local i
    local hold="${DATAPACKAGE_DATAFILE}"
    local id="${DATAPACKAGE_ID}"
    local ver="${DATAFIELD_VERSION}"
    local vers="${DATAPACKAGE_VERSIONS}"
    process_html LIST "VERSIONS.HEADER"
    for i in $vers ; do
        populate_item_data "${REPOSITORY_PATH}/${PATH_DATA}/${id}.${i}.${EXTENSION_DATA}"
        [[ "$DATAFIELD_VERSION" == "$ver" ]] && continue
        DATAITEM_ID="${id}.${i}"
        DATAITEM_NAME="$DATAITEM_ID"
        DATAITEM_TITLE="$DATAFIELD_TITLE $DATAFIELD_VERSION"
        DATAITEM_TEXT="$DATAITEM_TITLE"
        DATAPACKAGE_URL="${id}.${i}.${EXTENSION_HTML}"
        DATAITEM_TARGET="_self"
        DATAITEM_VALUE="$(url_relate $DATAPACKAGE_URL)"
        process_html LIST "VERSIONS.ITEM"
    done
    process_html LIST "VERSIONS.FOOTER"

    populate_item_data "${hold}"
}

# Invoked by template to list a set of URLs
function list_links
{
    local this
    local hold_id="$DATAITEM_ID"
    local hold_name="$DATAITEM_NAME"
    local hold_title="$DATAITEM_TITLE"
    local hold_text="$DATAITEM_TEXT"
    local hold_value="$DATAITEM_VALUE"
    local links="$hold_value"

    if [[ "$links" != "" ]] ; then
        process_html LIST "LINKS.HEADER"
        while [[ "$links" != "" ]] ; do
            this="${links%% *}"
            links="${links:$(( ${#this} + 1 ))}"
            [[ "${this:$(( ${#this} - 1 )):1}" == "," ]] && this="${this:0:$(( ${#this} - 1 ))}"
            DATAITEM_ID="URL"
            DATAITEM_NAME="URL"
            this="$(url_fix $this)"
            DATAITEM_TITLE="$this"
            DATAITEM_VALUE="$this"
            if [[ "${links:0:1}" == "(" ]] ; then
                DATAITEM_TEXT="${links%%)*})"
                links="${links:$(( ${#DATAITEM_TEXT} + 1 ))}"
            else
                DATAITEM_TEXT=""
            fi;
            url_relate $DATAITEM_VALUE >/dev/null
            this="$URL_RELATIVE"
            DATAITEM_TARGET="_blank"
            [[ "${URL_TITLE#*://}" == "$URL_TITLE" ]] && DATAITEM_TITLE="$URL_TITLE"
            if [[ "${this#*/}" == "$this" ]] ; then
                DATAITEM_TITLE="${REPOSITORY_TITLE} - ${DATAFIELD_TITLE}"
                DATAITEM_TARGET="_self"
            fi;
            DATAITEM_VALUE="$this"
            [[ "$this" != "" ]] && process_html LIST "LINKS.ITEM"
        done
        process_html LIST "LINKS.FOOTER"
    fi;
    DATAITEM_ID="$hold_id"
    DATAITEM_NAME="$hold_name"
    DATAITEM_TEXT="$hold_text"
    DATAITEM_TITLE="$hold_title"
    DATAITEM_VALUE="$hold_value"
}

# Invoked by template to list packages in a group
function list_group
{
    process_html LIST "GROUP.HEADER"
    [[ "$DATAGROUP_COUNT" != "0" ]] && {
        for package in $(sorted_package_list "$DATAGROUP_ID") ; do
            if [[ -e "$DATAGROUP_ID/$package" ]] ; then
                populate_item_data "${REPOSITORY_PATH}/${PATH_DATA}/${package%.*}.${EXTENSION_DATA}"
                DATAITEM_ID="${package%.*}"
                DATAITEM_NAME="$DATAITEM_ID"
                DATAITEM_TITLE="$DATAFIELD_TITLE"
                DATAITEM_VALUE="$DATAPACKAGE_URL"
                DATAITEM_TEXT=""
                DATAITEM_TARGET="_self"
                process_html LIST "GROUP.ITEM"
            fi
        done
    }
    clear_field_data
    process_html LIST "GROUP.FOOTER"
}

function list_index_each
{
    populate_group_data "$1"
    DATAITEM_ID="$DATAGROUP_ID"
    DATAITEM_NAME="$DATAGROUP_NAME"
    DATAITEM_TITLE="$DATAGROUP_TITLE"
    DATAITEM_VALUE="$DATAGROUP_URL"
    DATAITEM_TEXT="$DATAGROUP_COUNT"
    DATAITEM_TARGET="_self"
    process_html LIST "INDEX.ITEM"

}

# Invoked by template to list all groups in repository
function list_index
{
    process_html LIST "INDEX.HEADER"
    for_all_groups list_index_each
    process_html LIST "INDEX.FOOTER"
}

function list_compare_columns
{
    process_html LIST "COMPARE.COLUMNS.HEADER"
    local i=0
    local list="$COMPARE_LIST"
    local this
    while [[ "$list" != "" ]] ; do
        this="${list%%,*}"
        list="${list:$(( ${#this} + 1 ))}"
        repository_select "${REPO_ID[$this]}" QUIET
        process_html LIST "COMPARE.COLUMNS.ITEM"
    done
    repository_select "$REPOSITORY_CURRENT" QUIET
    process_html LIST "COMPARE.COLUMNS.FOOTER"
}

# Maybe later, I don;t have time right know. So will just populate them using
# the built in populate_item_data
function populate_item_from_csv
{
    clear_field_data
    PACKAGE_CSV=
    if [[ -e "${REPOSITORY_PATH}/${PATH_DATA}/${1}.${EXTENSION_DATA}" ]] ; then
        populate_item_data "${REPOSITORY_PATH}/${PATH_DATA}/${1}.${EXTENSION_DATA}"
    fi
}

function list_compare_package
{
    local i=0
    local list="$COMPARE_LIST"
    local id="$DATAPACKAGE_ID"
    local grp="$DATAGROUP_ID"
#    populate_group_data_from_list "$grp"
    clear_group_data
    process_html LIST "COMPARE.PACKAGE.HEADER"
#    clear_group_data
    local last_md5
    local last_sha
    local last_ver
    local last_id
    local waiting=yes
    local this
    (( INDENT += 2 ))
    while [[ "$list" != "" ]] ; do
        this="${list%%,*}"
        list="${list:$(( ${#this} + 1 ))}"
        repository_select "${REPO_ID[$this]}" QUIET
        populate_item_from_csv "${id}"
        if [[ "$DATAPACKAGE_ID" == "" ]] ; then
            message "${REPO_ID[$this]} Missing"
            process_html LIST "COMPARE.PACKAGE.MISSING"
        elif [[ "$waiting" == "no" ]] && [[ "$DATAFIELD_SHA" != "" ]] && [[ "$last_sha" != "" ]]; then
            if [[ "$DATAFIELD_SHA" == "$last_sha" ]] ; then
                message "${REPO_ID[$this]} Identical SHA"
                process_html LIST "COMPARE.PACKAGE.IDENTICAL"
            else
                message "${REPO_ID[$this]} Similar SHA mismatch"
                process_html LIST "COMPARE.PACKAGE.SIMILAR"
            fi
        elif [[ "$waiting" == "no" ]] && [[ "$DATAFIELD_MD5" == "$last_md5" ]] && [[ "$last_md5" != "" ]] ; then
            message "${REPO_ID[$this]} Identical MD5"
            process_html LIST "COMPARE.PACKAGE.IDENTICAL"
        elif  [[ "$waiting" == "no" ]] && [[ "$DATAFIELD_VERSION" == "$last_ver" ]] ; then
            message "${REPO_ID[$this]} Similar MD5 mismatch"
            process_html LIST "COMPARE.PACKAGE.SIMILAR"
        elif [[ "$last_id" == "" ]] ; then
            message "${REPO_ID[$this]} First/New"
            process_html LIST "COMPARE.PACKAGE.NEW"
        else
            message "${REPO_ID[$this]} Different"
            process_html LIST "COMPARE.PACKAGE.ITEM"
        fi
        if [[ "$DATAPACKAGE_ID" != "" ]] ; then
            last_sha="$DATAFIELD_SHA"
            last_md5="$DATAFIELD_MD5"
            last_ver="$DATAFIELD_VERSION"
            last_id="$DATAPACKAGE_ID"
            waiting=no
        fi;
    done
    (( INDENT -= 2 ))
    repository_select "$REPOSITORY_CURRENT" QUIET
    if [[ -e "${REPOSITORY_PATH}/${PATH_DATA}/${id}.${EXTENSION_DATA}" ]] ; then
        populate_item_data "${REPOSITORY_PATH}/${PATH_DATA}/${id}.${EXTENSION_DATA}"
    else
        clear_field_data
    fi;
    # populate_group_data_from_list "$grp"
    process_html LIST "COMPARE.PACKAGE.FOOTER"
}

function list_compare_group
{
    local have_url
    local list
    local this
    local i

    # [[ "$1" != "boot" ]] && return 0
    populate_group_data_from_list "$1"
    [[ "$DATAGROUP_COUNT" != "0" ]] && {
        message "Comparing \`$1' group with $DATAGROUP_COUNT packages."
        (( INDENT += 2 ))
        process_html LIST "COMPARE.GROUP.HEADER"
        for package in $(sorted_package_repo_list "$1") ; do
            if [[ -e "${REPOSITORY_PATH}/${PATH_DATA}/${package%.*}.${EXTENSION_DATA}" ]] ; then
                populate_item_data "${REPOSITORY_PATH}/${PATH_DATA}/${package%.*}.${EXTENSION_DATA}"
                have_url=true
            else
                have_url=false
                clear_field_data
                list="$COMPARE_LIST"
                while [[ "$list" != "" ]] ; do
                    this="${list%%,*}"
                    list="${list:$(( ${#this} + 1))}"
                    if [[ -e "${REPO_PATH[$this]}/${PATH_DATA}/${package%.*}.${EXTENSION_DATA}" ]] ; then
                        list=""
                        populate_item_data "${REPO_PATH[$this]}/${PATH_DATA}/${package%.*}.${EXTENSION_DATA}"
                    fi
                done
            fi
            message "Package \`${package%.*}'"
            DATAITEM_ID="${package%.*}"
            DATAITEM_NAME="$DATAITEM_ID"
            DATAITEM_TITLE="$DATAFIELD_TITLE"
            DATAITEM_VALUE="$DATAPACKAGE_URL"
            DATAITEM_TEXT=""
            DATAITEM_TARGET="_self"
            if [[ "$have_url" == true ]] ; then
                process_html LIST "COMPARE.GROUP.ITEM"
            else
                process_html LIST "COMPARE.GROUP.NOURL"
            fi
        done
        (( INDENT -= 2 ))
        clear_field_data
        populate_group_data_from_list "$1"
        process_html LIST "COMPARE.GROUP.FOOTER"
    }
}

function list_compare_index
{
    process_html LIST "COMPARE.INDEX.HEADER"
    for_all_repo_groups list_compare_group

    process_html LIST "COMPARE.INDEX.FOOTER"
}

function list_rss
{
    local prev="${REPOSITORY_PATH}/${PATH_HTML}/rss.${EXTENSION_XML}"
    local max=$MAX_RSS
    [[ ! -f "$prev" ]] && local max=$MAX_RSS_NEW
    local count=0
    process_html LIST "RSS.HEADER"
    local hold="$PWD"
    system_cd "$REPOSITORY_PATH"
    local item
    local package

    clear_field_data
    for item in $(ls -1t */*.zip | head -n $max) ; do
        populate_group_data "${item%%/*}"
        package="${item##*/}"
        package="${package%.*}"
        populate_item_data "${REPOSITORY_PATH}/${PATH_DATA}/${package%.*}.${EXTENSION_DATA}"
        DATAPACKAGE_URL="${REPOSITORY_URL}/${PATH_HTML}/${DATAPACKAGE_URL}"
        DATAPACKAGE_FILEURL="${REPOSITORY_URL}/${item%%/*}/${package}.${DATAPACKAGE_FILEURL##*.}"
        DATAGROUP_URL="${REPOSITORY_URL}/${PATH_HTML}/${DATAGROUP_URL}"
        if [[ -f "$prev" ]] ; then
            cat "$prev" | grep -a  -m 1 -i ">$DATAPACKAGE_GUID</guid>"  >/dev/null 2>&1 && break
        fi;
        log "RSS $item"
        process_html LIST "RSS.ITEM"
        clear_field_data
        (( count++ ))
    done;
    if [[ $count -lt $MAX_RSS ]] && [[ -f "$prev" ]] ; then
        cat "$prev" | grep -a  -A 100000 -m $(($max - $count)) -i '<item>' | grep -a  -B 100000 -i '</item>'  | grep -a  -v "^--$"
    fi
    cd "$HOLD"
    process_html LIST "RSS.FOOTER"
}


# Parse a template section, parsing all tags and performing appropriate actions
function process_html
{
    local line
    local ret
    local cmd
    local orig
    local opt
    local psafe
    local pstrip
    local this
    local filename
    local tpath

    while IFS="" read -r line ; do
        if [[ "$line" == "" ]] ; then
            :; # Ignore blank lines
        elif [[ "${line//<!--/}" == "$line" ]] ; then
            echo "$line"
        else
            ret=
            while [[ "$line" != "" ]] ; do
                local pre="${line%%<!--*}"
                if [[ "$pre" == "$line" ]] ; then
                    ret="${ret}${pre}"
                    line=
                else
                    line="${line:$(( ${#pre} + 4 ))}"
                    cmd="${line%%>*}"
                    [[ "$cmd" == "$line" ]] && show_error 1 "unclosed command in \`$1:$2'"
                    line="${line#*>}"
                    cmd="${cmd%%--*}"
                    orig="$cmd"
                    opt="${cmd#*:}"
                    cmd=$(caseUpper "${cmd}")
                    [[ "$opt" == "$cmd" ]] && opt= || cmd="${cmd%%:*}"
                    [[ "${cmd}" != "COMMENT" ]] && opt=$(caseUpper "${opt}")
                    psafe=
                    pstrip=
                    [[ "${cmd:0:1}" == "@" ]] && { cmd="${cmd:1}"; psafe=yes; }
                    [[ "${cmd:0:1}" == "%" ]] && { cmd="${cmd:1}"; pstrip=yes; }
                    this=
                    case "${cmd}" in
                        COMMENT)
                            this="<!-- $opt -->"
                            ;;
                        HTML)
                            this=$(process_html HTML "$opt")
                            ;;
                        REPOSITORY|REPO)
                            this="REPOSITORY_${opt}"
                            this="${!this}"
                            ;;
                        CONTACT)
                            this="${CONTACT}"
                            ;;
                        WEBMASTER)
                            this="${WEBMASTER}"
                            ;;
                        LSM)
                            this="DATAFIELD_${opt}"
                            this="${!this}"
                            ;;
                        GROUP)
                            this="DATAGROUP_${opt}"
                            this="${!this}"
                            ;;
                        ITEM)
                            this="DATAITEM_${opt}"
                            this="${!this}"
                            ;;
                        PACKAGE)
                            this="DATAPACKAGE_${opt}"
                            this="${!this}"
                            if [[ "$this" == "" ]] ; then
                                this="DATAFIELD_${opt}"
                                this="${!this}"
                            fi;
                            ;;
                        LIST)
                            if [[ "$opt" == "LSM" ]] || [[ "$opt" == "PACKAGE" ]] ; then
                                this=$(list_package)
                            elif [[ "$opt" == "LINKS" ]] ; then
                                this=$(list_links)
                            elif [[ "$opt" == "VERSIONS" ]] ; then
                                this=$(list_versions)
                            elif [[ "$opt" == "GROUP" ]] ; then
                                this=$(list_group)
                            elif [[ "$opt" == "INDEX" ]] ; then
                                this=$(list_index)
                            elif [[ "$opt" == "COMPARE.INDEX" ]] ; then
                                this=$(list_compare_index)
                            elif [[ "$opt" == "COMPARE.GROUP" ]] ; then
                                this=$(list_compare_group)
                            elif [[ "$opt" == "COMPARE.PACKAGE" ]] ; then
                                this=$(list_compare_package)
                            elif [[ "$opt" == "COMPARE.COLUMNS" ]] ; then
                                this=$(list_compare_columns)
                            elif [[ "$opt" == "RSS" ]] ; then
                                this=$(list_rss)
                            else
                                this="<!-- Unknown list type $opt -->"
                            fi
                            ;;
                        DATE)
                            if [[ "$opt" == "STAMP" ]] ; then
                                this="$(external DATESTAMP)"
                            elif [[ "$opt" == "YEAR" ]] ; then
                                this="$(external DATEYEAR)"
                            elif [[ "$opt" == "RSS" ]] ; then
                                this="$(external DATERSS)"
                            else
                                this="<!-- Unknown date type $opt -->"
                            fi;
                            ;;
                        DATESTAMP)
                            this="$(external DATESTAMP)"
                            ;;
                        "IF EXIST"|"IF EXISTS"|IFEXIST|IFEXISTS)
                            filename=$(caseLower "${opt%%:*}")
                            opt="${opt:$(( ${#filename} + 1 ))}"
                            tpath="${REPOSITORY_PATH}/${PATH_HTML}"
                            if [[ "${filename:0:3}" == "../" ]] ; then
                                while [[ "${filename:0:3}" == "../" ]] && [[ "$tpath" != "" ]] ; do
                                    filename="${filename#*/}"
                                    tpath="${tpath%/*}"
                                done
                            elif [[ "${filename:0:1}" == "/" ]] ; then
                                tpath=""
                            fi
                            if [[ "$opt" == "" ]] ; then
                                log "Template error \`${cmd}:${filename}:${opt}'"
                                this="${this}<!-- TEMPLATE ERROR -->"
                            elif [[ -e "$tpath/$filename" ]] ; then
                                line="<!--${opt}-->${line}"
                            else
                                this=""
                            fi
                            ;;
                        NULL)
                            this=""
                            ;;
                        BLANKLINE)
                            this=""
                            echo ""
                            ;;
                        "IF SET"|IFSET)
                            # filename here is not, just a variable name
                            filename=$(caseUpper "${opt%%:*}")
                            opt="${opt:$(( ${#filename} + 1 ))}"
                            if [[ "$opt" == "" ]] || [[ "$filename" == "" ]] ; then
                                log "Template error \`${cmd}:${variable}:${opt}'"
                                this="${this}<!-- TEMPLATE ERROR -->"
                            elif [[ "${!filename}" != "" ]] ; then
                                line="<!--${opt}-->${line}"
                            else
                                this=""
                            fi
                            ;;
                        *)
                            # Pass through unknown as comment block
                            this="<!--${orig}-->"
                    esac
                    [[ "${psafe}"  == "yes" ]] && this=$(sanitize_html "${this}")
                    [[ "${pstrip}" == "yes" ]] && this=$(stripLower "${this}")
                    ret="${ret}${pre}${this}"
                fi
            done
            [[ "$ret" != "" ]] && echo "$ret"
            ret=
        fi
    done<<<"$(getTemplate $1:$2)"
}

# Create the META_DATA text file for a package
function make_item_data
{
    local dest="${2#*/}"
    dest="${dest%.*}"
    dest=$(safe_filename "${dest//\//$VERSIONING}")
    local group="${2%%/*}"
    local result
    local new

    # Extract LSM from Begin3 to just befor end
    unzip -Cc "${2}" "APPINFO/${1%.*}.lsm"| grep -a  -a -A 10000 -i "^Begin3" | grep -a  -a -B 10000 -i "^end" | grep -a  -aiv "^end">"${PATH_TEMP}/${dest}.tmp" || show_error "extracting LSM from \`$2'"

    local margin=7
    local line
    local colon
    while IFS="" read -r line ; do
        if [[ "${line:0:1}" != " " ]] ; then
            colon="${line%%:*}"
            [[ "$colon" != "$line" ]] && [[ ${#colon} -gt $margin ]] && margin=${#colon}
        fi
    done<"${PATH_TEMP}/${dest}.tmp"
    (( margin += 2 ))

    echo "Group: ${group}">>"${PATH_TEMP}/${dest}.tmp"
    if [[ "$FUNCTION_CRC" != "" ]] ; then
        result=$(external CRC "$2" 2>&1)
        echo "CRC: $result">>"${PATH_TEMP}/${dest}.tmp"
    fi
    if [[ "$FUNCTION_MD5" != "" ]] ; then
        result=$(external MD5 "$2" 2>&1)
        echo "MD5: $result">>"${PATH_TEMP}/${dest}.tmp"
    fi
    if [[ "$FUNCTION_SHA" != "" ]] ; then
        result=$(external SHA "$2" 2>&1)
        echo "SHA: $result">>"${PATH_TEMP}/${dest}.tmp"
    fi
    echo "End">>"${PATH_TEMP}/${dest}.tmp"

    while IFS="" read -r line ; do
        line=$(noTabs "$line")
        if [[ "${line:0:1}" != " " ]] ; then
            colon="${line%%:*}"
            if [[ "$colon" != "$line" ]] ; then
                new="${colon}:${SPACES}"
                new="${new:0:$margin}"
                line="${line:$(( ${#colon} + 1 ))}"
                while [[ "${line:0:1}" == " " ]] ; do
                    line="${line:1}"
                done
                if [[ "$line" == "-" ]] || [[ "$line" == "?" ]] ; then
                    line=""
                fi
                echo "${new}${line}"
            else
                echo "$line"
            fi;
        else
            echo "$line"
        fi
    done<"${PATH_TEMP}/${dest}.tmp">"${PATH_TEMP}/${dest}.lsm"

    # cat "${PATH_TEMP}/${dest}.lsm"

    system_rm "${PATH_TEMP}/${dest}.tmp"
    system_mkdir "${REPOSITORY_PATH}/${PATH_DATA}"

    place_file "${PATH_TEMP}/${dest}.lsm" "${REPOSITORY_PATH}/${PATH_DATA}/${dest}.${EXTENSION_DATA}"
    return 0
}

# Create HTML file for a package
function make_item_data_html
{
    local dest="${2#*/}"
    dest="${dest%.*}"
    dest=$(safe_filename "${dest//\//$VERSIONING}")
    clear_field_data

    populate_item_data "${REPOSITORY_PATH}/${PATH_DATA}/${dest}.${EXTENSION_DATA}"
    populate_group_data "${DATAFIELD_GROUP}"

    process_html HTML PACKAGE>"${PATH_TEMP}/${dest}.tmp"

    system_mkdir "${REPOSITORY_PATH}/${PATH_HTML}"

    place_file "${PATH_TEMP}/${dest}.tmp" "${REPOSITORY_PATH}/${PATH_HTML}/${dest}.${EXTENSION_HTML}"
    return 0
}

# Process all packages marked for data rebuild.
function rebuild_item_data
{
    local pkg=$(ls -1d */"$1" 2>&1 | head -n 1)
    if [[ -d "$pkg" ]] ; then
        log "Create \`$pkg' versioned package data"
        (( INDENT += 2 ))
        for i in ${pkg}/* ; do
            [[ -f "$i" ]] && make_item_data "${pkg##*/}" "$i"
        done
        local latest="$(file_latest ${pkg} 2>/dev/null)"
        latest="${latest#*/}"
        latest="${latest//\//${VERSIONING}}"
        latest="${latest%.*}.${EXTENSION_DATA}"
        if [[ -f "${REPOSITORY_PATH}/${PATH_DATA}/${latest}" ]] ; then
            local hold="$PWD"
            system_cd "${REPOSITORY_PATH}/${PATH_DATA}"
            local link="${pkg#*/}"
            link="${link%%/*}.${EXTENSION_DATA}"
            if [[ -f "${link}" ]] || [[ -L "${link}" ]] ; then
                system_rm "${link}"
            fi
            system_ln "${latest}" "${link}"
            cd "$hold"
        fi;
        (( INDENT -= 2 ))
    elif [[ -e "$pkg" ]] ; then
        log "Create \`$pkg' package data"
        make_item_data "${pkg##*/}" "$pkg"
    fi
}

# Process all packages marked for data rebuild to create HTML pages
function rebuild_item_data_html
{
    local pkg=$(ls -1d */"$1" 2>&1| head -n 1)
    if [[ -d "$pkg" ]] ; then
        log "Create \`$pkg' versioned package html data"
        (( INDENT += 2 ))
        for i in ${pkg}/* ; do
            [[ -f "$i" ]] && {
                make_item_data_html "${pkg##*/}" "$i"
                message "Version: ${DATAFIELD_VERSION} (${DATAFIELD_ENTEREDDATE})"
            }
        done
        local latest="$(file_latest ${pkg} 2>/dev/null)"
        latest="${latest#*/}"
        latest="${latest//\//${VERSIONING}}"
        latest="${latest%.*}.${EXTENSION_HTML}"
        if [[ -f "${REPOSITORY_PATH}/${PATH_HTML}/${latest}" ]] ; then
            local hold="$PWD"
            system_cd "${REPOSITORY_PATH}/${PATH_HTML}"
            local link="${pkg#*/}"
            link="${link%%/*}.${EXTENSION_HTML}"
            if [[ -f "${link}" ]] || [[ -L "${link}" ]] ; then
                system_rm "${link}"
            fi
            system_ln "${latest}" "${link}"
            cd "$hold"
        fi;
        (( INDENT -= 2 ))
    elif [[ -e "$pkg" ]] ; then
        log "Create \`$pkg' package html data"
        make_item_data_html "${pkg##*/}" "$pkg"
    fi

    clear_field_data
}

# Slower internal clone of external buildidx utility core functionality.
function internal_buildidx
{
    local group="$1"
    [[ ! -d "${REPOSITORY_PATH}/$group" ]] && show_error 1 "Invalid \`$group' group"
    system_rm "${PATH_TEMP}/index.lst"
    system_touch "${PATH_TEMP}/index.lst"
    local buildtime=$(external STAT "${PATH_TEMP}/index.lst")
    local count=$(package_list "$group" | wc -l)
    local package
    local crc
    echo "FD-REPOv1|Build time: ${buildtime}|${group}|${count}" | tr '|' '\t' >>"${PATH_TEMP}/index.lst"
    [[ $count -ne 0 ]] && {
        for package in $(package_list "$group") ; do
            if [[ -e "$group/$package" ]] ; then
                populate_item_data "${REPOSITORY_PATH}/${PATH_DATA}/${package%.*}.${EXTENSION_DATA}" dumb
                crc=$(echo "$DATAFIELD_CRC" | tr "[:lower:]" "[:upper:]")
                [[ "$crc" == "" ]] && CRC="0"
                DATAFIELD_DESCRIPTION=$(sanitize_index "$DATAFIELD_DESCRIPTION")
                echo "${package%.*}|${DATAFIELD_VERSION}|${DATAFIELD_DESCRIPTION}|${crc}" | tr '|' '\t' >>"${PATH_TEMP}/index.lst"
            fi
        done
    }

    place_file "${PATH_TEMP}/index.lst" "${REPOSITORY_PATH}/$group/index.lst" && {
        gzip -9c "${REPOSITORY_PATH}/$group/index.lst" >"${PATH_TEMP}/index.gz" || show_error 1 "Unable to compress \`${REPOSITORY_PATH}/${group}/listing.txt'"
        system_mv "${PATH_TEMP}/index.gz" "${REPOSITORY_PATH}/$group/index.gz"
    }
    return 0
}

# Splice group into listing file
function index_splice
{
    local group="$1"
    local build=$(external STAT "${REPOSITORY_PATH}/$group/index.lst")
    build=$(external EPOCH_TO_DATE $build)
    local line
    local first
    local second
    echo>>"${PATH_TEMP}/listing.txt"
    echo "*** Repository '$group' - build time: ${build}">>"${PATH_TEMP}/listing.txt"
    echo>>"${PATH_TEMP}/listing.txt"
    cat "${REPOSITORY_PATH}/$group/index.lst" | grep -a  -iv "^FD-REPOv1" | while IFS="" read -r line ; do
        if [[ "$line" != "" ]] ; then
            first=$(echo "$line" | tr "\t" '|' | cut -d "|" -f -2 | tr "|" " ")
            second=$(echo "$line" | tr "\t" '|' | cut -d "|" -f 3)
            echo "$first - $second">>"${PATH_TEMP}/listing.txt"
        fi
    done
    echo >>"${PATH_TEMP}/listing.txt"
}

# Create repository listing file from pre-existing indexes
function internal_lising
{
    system_rm "${PATH_TEMP}/listing.txt"
    system_touch "${PATH_TEMP}/listing.txt"
    for_all_groups index_splice
}

# function to create index files using external utility
function external_index
{
    local hold="$PWD"
    system_cd "${PATH_TEMP}"
    local group="$1"
    log "Index \`${group}' group"
    external BUILDIDX "$(path_relate ${REPOSITORY_PATH}/$group)" || show_error 1 "Index failed."
    cat "${REPOSITORY_PATH}/$group//index.lst" | sed "s|$(path_relate ${REPOSITORY_PATH}/$group)|$group|g" >"${PATH_TEMP}/index.tmp.lst"
    place_file "${PATH_TEMP}/index.tmp.lst" "${REPOSITORY_PATH}/$group/index.lst" && {
        gzip -9c "${REPOSITORY_PATH}/$group/index.lst" >"${PATH_TEMP}/index.gz" || show_error 1 "Unable to compress \`${REPOSITORY_PATH}/${group}/listing.txt'"
        system_mv "${PATH_TEMP}/index.gz" "${REPOSITORY_PATH}/$group/index.gz"
    }
    cd "$hold"
    return 0
}

# Perform an action for all groups in proper order
function for_all_groups
{
    # First pass in order of GROUP_ORDER
    local list="${GROUP_ORDER}"
    local group
    while [[ "$list" != "" ]] ; do
        group="${list%%;*}"
        list="${list:$(( ${#group} + 1 ))}"
        if [[ "$group" != "" ]] ; then
            if [[ -d "${REPOSITORY_PATH}/$group" ]] ; then
                $@ "$group"
            else
                message "Invalid (or missing) group ID \`$group' in GROUP_ORDER"
            fi
        fi;
    done

    # Second pass not not in GROUP_ORDER
    local list=";${GROUP_ORDER};"
    local i
    for i in "${REPOSITORY_PATH}/"* ; do
        group="${i##*/}"
        if [[ -d "$i" ]] && [[ "${group}" != "$PATH_DATA" ]] && [[ "$group" != "$PATH_HTML" ]] && [[ "${list//;${group};/}" == "$list" ]] ; then
            $@ "$group"
        fi
    done
}

function info_group_list
{

    local p
    local t
    for p in $(cat "$1"* | grep -a  -i ^Group | cut -d ':' -f 2 | tr -d ' ' ) ; do
        t=$(echo $p | tr -d "[:cntrl:]")
        [[ "$p" == "$t" ]] && echo "$p"
    done | sort -u

}

function info_packages_list
{

    local p
    local t
    for p in $( grep -a  -i "^Group:.*${2}" "$1"* | cut -d ':' -f 1 | sort -u ) ; do
        t="${p##*/}"
        t="${t%.*}"

        [[ "$t" != "" ]] && [[ "${t//./}" == "$t" ]] && echo "$2/${t}.zip"
    done


}

# Perform an action for all groups in proper order in all repos
function for_all_repo_groups
{
    # First pass in order of GROUP_ORDER
    system_rm "${PATH_TEMP}/packages.tmp" "${PATH_TEMP}/packagelist.tmp"
    system_touch "${PATH_TEMP}/packages.tmp"
    local tgrp=";${GROUP_ORDER};"
    local list="${REPOSITORY_NUMBER},$COMPARE_LIST"
    local i
    local this
    local hold
    local p
    local group
    local once=yes

    if [[ "$COMPARE_DELETED" == "yes" ]] ; then
        message "Detecting all groups"
    fi

    while [[ "$list" != "" ]] ; do
        this="${list%%,*}"
        list="${list:$(( ${#this} + 1 ))}"

        if [[  "${REPO_ID[$this]}" == "$REPOSITORY_ID" ]] ; then
            [[ "$once" != "yes" ]] && continue
            once=no
        fi

        (( INDENT +=2 ))
        message "Scanning \`${REPO_ID[$this]}'"
        (( INDENT -=2 ))

        if [[ "$COMPARE_DELETED" != "yes" ]] ; then
            for i in "${REPO_PATH[$this]}/"* ; do
                group="${i##*/}"
                if [[ -d "$i" ]] && [[ "${group}" != "$PATH_DATA" ]] && [[ "$group" != "$PATH_HTML" ]] ; then
                    [[ "${tgrp//;${group};/}" == "$tgrp" ]] && tgrp="${tgrp};${i##*/};"
                    hold="$PWD"
                    system_cd "${REPO_PATH[$this]}"
                    ls -a1d "${group}/"*.* 2>/dev/null | grep -a  -iv "/index.lst$" | grep -a  -iv "/index.gz$" >>"${PATH_TEMP}/packages.tmp"
                    cd "$hold"
                fi
            done
        else
            for i in $(info_group_list "${REPO_PATH[$this]}/pkg-info/")  ; do
                group=$(caseLower $i)
                if [[ "${tgrp//;${group};/}" == "$tgrp" ]] ; then
                    tgrp="${tgrp};${group};"
                fi
                info_packages_list "${REPO_PATH[$this]}/pkg-info/" "${group}" >>"${PATH_TEMP}/packages.tmp"
            done
        fi
    done

    cat "${PATH_TEMP}/packages.tmp" >"${PATH_TEMP}/packagelist.tmp"
    system_rm "${PATH_TEMP}/packages.tmp"
    system_touch "${PATH_TEMP}/packages.tmp"
    for i in $(cat "${PATH_TEMP}/packagelist.tmp") ; do
        p="${i##*/}"
        grep -a  -m 1 "/${p%.*}.zip" "${PATH_TEMP}/packagelist.tmp">>"${PATH_TEMP}/packages.tmp"
    done

    cat "${PATH_TEMP}/packages.tmp" | sort -u >"${PATH_TEMP}/packagelist.tmp"

    if [[ "$COMPARE_DELETED" == "yes" ]] ; then
        message "Groups found: $(echo ${tgrp//;;/,} | tr -d ';')"
    fi

    list="$tgrp"
    while [[ "$list" != "" ]] ; do
        group="${list%%;*}"
        list="${list:$(( ${#group} + 1 ))}"
        if [[ "$group" != "" ]] ; then
            $@ "$group"
        fi;
    done

    system_rm "${PATH_TEMP}/packages.tmp" "${PATH_TEMP}/packagelist.tmp"

}

# Create index.lst & index.gz for a group
function rebuild_item_index
{
    if [[ "${FUNCTION_BUILDIDX}" != "" ]] ; then
        [[ "$INDEX_ONCE" == true ]] && return 0
        INDEX_ONCE=true
        local name="${FUNCTION_BUILDIDX%% *}"
        log "Indexing all package groups with \`${name##*/}' utility"
        for_all_groups external_index

    else
        log "Index \`$1' group"
        internal_buildidx "$1"
    fi
}

# Create the HTML file for a group
function rebuild_item_index_html
{
    log "Index \`$1' group html"
    clear_field_data
    populate_group_data "$1"
    process_html HTML GROUP >"${PATH_TEMP}/$1.${EXTENSION_HTML}"
    place_file "${PATH_TEMP}/$1.${EXTENSION_HTML}" "${REPOSITORY_PATH}/${PATH_HTML}/${PREFIX_GROUP}${1}.${EXTENSION_HTML}"
    return 0
}

# Compare repositories
function repository_compare
{
    [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
    if [[ "$FORCE" != "yes" ]] ; then
        local cstamp=$(external STAT "${REPOSITORY_PATH}/${PATH_HTML}/comparison.${EXTENSION_HTML}")
        local lstamp=$(external STAT "${REPOSITORY_PATH}/listing.csv")
        if [[ $lstamp -lt $cstamp ]] ; then
            message "Existing \`comparison.${EXTENSION_HTML}' is newer than \`listing.csv', skipped."
            return 0
        fi
    fi
    REPOSITORY_CURRENT="$REPOSITORY_ID"
    if [[ "$1" == "all" ]] || [[ "$1" == "" ]] ; then
        log "Compare \`$REPOSITORY_CURRENT' with all repositories"
     else
        log "Compare \`$REPOSITORY_CURRENT' with \`$1' repositories"
    fi
    repository_protected && return 0
    create_template_files
    system_rm "${PATH_TEMP}/compare.tmp"

    local order=
    local shown=
    local list
    local this
    local i

    if [[ "$1" == "all" ]] || [[ "$1" == "" ]]; then
        i=0
        while [[ "${REPO_ID[$i]}" != "" ]]; do
            [[ "$order" != "" ]] && order="${order},${i}" || order="${i}"
            [[ "$shown" != "" ]] && shown="${shown},${REPO_ID[${i}]}" || shown="${REPO_ID[${i}]}"
            (( i++ ))
        done
        message "Repositories: \`$shown'"
    else
        i=0
        list="$1"
        while [[ "$list" != "" ]] ; do
            this="${list%%,*}"
            list="${list:$(( ${#this} + 1 )) }"
            i=0
            while [[ "${REPO_ID[$i]}" != "" ]] && [[ "${REPO_ID[$i]}" != "$this" ]]; do
                (( i++ ))
            done
            [[ "${REPO_ID[$i]}" != "$this" ]] && repository_select "$this"
            [[ "$order" != "" ]] && order="${order},${i}" || order="${i}"
        done
    fi

    if [[ false == true ]] ; then

        # Since the CSV is not currently used for comparison, this is not necessary

        list=$order

        local header
        local hheader
        local forder
        local field
        local j


        while [[ "$list" != "" ]] ; do
            forder=
            this="${list%%,*}"
            list="${list:$(( ${#this} + 1 )) }"
            repository_select "${REPO_ID[$this]}" QUIET
            [[ ! -f "${REPOSITORY_PATH}/listing.csv" ]] && show_error 1 "Repository \`$REPOSITORY_ID' missing \`listing.csv'"
            header=$(cat "${REPOSITORY_PATH}/listing.csv" | head -n 1 | tr "[:lower:]" "[:upper:]")
            header=$(caseUpper "$header")
            message "$this: $header"
            if [[ "$hheader" == "" ]] ; then
                hheader="$header"
                i=0
                while [[ "$header" != "" ]] ; do
                    field="${header%%,*}"
                    header="${header:$(( ${#field} + 1 )) }"
                    j=0
                    while [[ "${LSM_ID[$j]}" != "" ]] && [[ "$(caseUpper ${LSM_ID[$j]})" != "$field" ]] ; do
                        (( j++ ))
                    done
                    if [[ "${LSM_ID[$j]}" != "" ]] ; then
                         forder="${forder}${j},"
                    else
                        [[ "$field" == "ID" ]] && forder="${forder}ID," || forder="${forder}NULL,"
                    fi
                done
                message "Field order: ${forder%,*}"
            elif [[ "$hheader" != "$header" ]] ; then
                 show_error 1 "CSV header mismatch in \`${REPO_ID[$this]}'. Force CSV rebuild!"
            fi;
        done;
        message ""

        COMPARE_FIELDS="$forder"
    fi

    COMPARE_LIST="$order"


    repository_select "$REPOSITORY_CURRENT" QUIET

    process_html HTML COMPARISON >"${PATH_TEMP}/compare.tmp"

    place_file "${PATH_TEMP}/compare.tmp" "${REPOSITORY_PATH}/${PATH_HTML}/comparison.${EXTENSION_HTML}"

    (( INDEND -= 2 ))
    return 0
}

# Create the main listing.txt & listing.gz for the repository
function rebuild_main_index
{
    [[ "${REBUILD_INDEX}" == "" ]] && return 0
    if [[ "${FUNCTION_BUILDIDX}" == "" ]] ; then
        log "Generate \`$REPOSITORY_ID' main index"
        internal_lising
    fi

    [[ ! -e "${PATH_TEMP}/listing.txt" ]] && show_error 1 "Main temporary index \`${PATH_TEMP}/listing.txt' missing."
    cat "${PATH_TEMP}/listing.txt" | sed "s|${REPOSITORY_PATH}||g" >"${PATH_TEMP}/listing.tmp" || show_error 1 "Failed to adjust \`${REPOSITORY_PATH}/listing.txt'"

    place_file "${PATH_TEMP}/listing.tmp" "${REPOSITORY_PATH}/listing.txt" && {
        gzip -9 "${PATH_TEMP}/listing.txt" || show_error 1 "Unable to compress \`${PATH_TEMP}/listing.txt'"
        system_mv "${PATH_TEMP}/listing.txt.gz" "${REPOSITORY_PATH}/listing.gz"
    }
    system_rm "${PATH_TEMP}/listing.tmp" "${PATH_TEMP}/listing.txt" "${PATH_TEMP}/listing.txt.gz"
    return 0
}

# Create the main HTML index page
function rebuild_main_index_html
{
    [[ "${REBUILD_INDEX}" == "" ]] && return 0
    log "Generate \`$REPOSITORY_ID' main html index"
    clear_field_data
    populate_group_data "$1"
    process_html HTML INDEX >"${PATH_TEMP}/index.${EXTENSION_HTML}"
    place_file "${PATH_TEMP}/index.${EXTENSION_HTML}" "${REPOSITORY_PATH}/${PATH_HTML}/index.${EXTENSION_HTML}"
    return 0
}

# Create the main listing.csv file
function rebuild_main_csv
{
    [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
    if [[ -e "${REPOSITORY_PATH}/listing.csv" ]] ; then
        [[ "${REBUILD_INDEX}" == "" ]] && [[ "$FORCE" != "yes" ]] && return 0
    fi;
    log "Build \`$REPOSITORY_ID' package CSV"
    repository_protected && return 0
    local j=0
    local s="id"
    while [[ "${LSM_ID[$j]}" != "" ]] ; do
        if [[ "${CSV_EXCLUDE//;$(caseLower ${LSM_ID[$j]});/}" == "${CSV_EXCLUDE}" ]] ; then
            s="${s},$(caseLower ${LSM_ID[$j]})"
        fi
        (( j++ ))
    done
    echo "$s">"${PATH_TEMP}/listing.csv"

    local fields="$s"
    local list
    local x

    for i in "$REPOSITORY_PATH/$PATH_DATA/"*.${EXTENSION_DATA} ; do
        s="${i##*/}"
        s="${s%.*}"
        if [[ "${s%.*}" == "${s}" ]] ; then
            list="${fields#*,}"
            while [[ "${list}" != "" ]] ; do
                j="${list%%,*}"
                list="${list:$(( ${#j} + 1 ))}"
                x=$(grep -a  -i "^${j}:" "$i" | tr '"' "'" | cut -d ':' -f 2- | tr "\t" ' ' | tr -s " " | tr -d "[:cntrl:]" || echo )
#                local x=$(echo "$x" | tr "\t" ' ' | tr -s " " | tr -d "[:cntrl:]")
                while [[ "${x:0:1}" == " " ]] ; do
                    x="${x:1}"
                done
                [[ "$x" != "${x//,/}" ]] && local x=\"${x}\"
                s="${s},${x}"
            done
            echo "$s">>"${PATH_TEMP}/listing.csv"
        fi
    done

    place_file "${PATH_TEMP}/listing.csv" "$REPOSITORY_PATH/listing.csv"
    return 0
}

# When template is newer that static CSS files, recreate them.
function create_template_files
{
    local line
    local name
    local dstamp
    local tstamp
    local target

    system_mkdir "${PATH_HTML}"
    if [[ -L "$TEMPLATE" ]] ; then
        target=$(link_target "$TEMPLATE")
        tstamp=$(external STAT "$target")
    else
        tstamp=$(external STAT "$TEMPLATE")
    fi

    for line in $(grep -a  -i '^# FILE:' "$TEMPLATE" | cut -d " " -f 2) ; do
        name="${REPOSITORY_PATH}/${PATH_HTML}/$(caseLower $line | cut -d ':' -f 2 )"
        dstamp=$(external STAT "$name")
        if [[ ! -e "$name" ]] || [[ $tstamp -gt $dstamp ]] ; then
            log "Create raw file \`$(caseLower $line | cut -d ':' -f 2 )'"
            getTemplate "$line">"${PATH_TEMP}/${name##*/}"
            place_file "${PATH_TEMP}/${name##*/}" "$name" || system_touch "$name"
        fi;
    done
    return 0
}

# Generic rebuild list processor,
function rebuild_items
{
    local class="REBUILD_$(caseUpper ${1})"
    local func="rebuild_item_$(caseLower ${2})"
    local list="${!class}"
    local item

    while [[ "$list" != "" ]] ; do
        item="${list%%;*}"
        list="${list#*;}"
        [[ "$list" == "$item" ]] && list=""
        item="${item:1}"
        $func "$item"
    done
}

# Remove obsolete files
function obsolete_scan
{
    [[ "${1//\*/}" != "$1" ]] && return 0
    local name
    local extension
    local skip
    local version
    local check
    local search

    while [[ "$1" != "" ]] ; do
        name="${1%.*}"
        name="${name##*/}"
        extension="${1##*.}"
        skip=false
        if [[ "$extension" == "${EXTENSION_HTML}" ]] ; then
            if [[ "$name" == "index" ]] || [[ "$name" == "comparison" ]] ; then
                skip=true # Ignore index and existing group html files
            elif [[ -d "${REPOSITORY_PATH}/${name:${#PREFIX_GROUP}}" ]] ; then
                skip=true
            fi
        fi

        if [[ "$skip" == "false" ]] ;  then
            version="${name#*${VERSIONING}}"
            [[ "$version" != "$name" ]] && name="${name%%${VERSIONING}*}" || version=

            skip=true
            # Not versioned
            check=$(find_package ${name})
            if [[ "$check" == "" ]] ; then
                skip=false
            elif [[ "$version" != "" ]] ; then
                if [[ ! -d "${check%.*}" ]] ; then
                    skip=false
                else
                    skip=false
                    for search in "${check%.*}/$version."* ; do
                        [[ "$search" != "${check%.*}/$version.*" ]] && skip=true
                    done
                    for search in "${check%.*}/$(safe_filename $version)."* ; do
                        [[ "$search" != "${check%.*}/$(safe_filename $version).*" ]] && skip=true
                    done
                fi;
            fi
        fi
        [[ ! -e "$1" ]] && skip=false

        if [[ "$skip" == "false" ]] ; then
            log "Remove obsolete reference file \`${1##*/}'"
            system_rm "$1"
        fi
        shift
    done
}

# Remove all obsolete files
function obsolete_scan_all
{
    [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
    message "Removing obsolete reference files"
    repository_protected && return 0
    (( INDENT += 2 ))
    obsolete_scan "${REPOSITORY_PATH}/${PATH_DATA}/"*".${EXTENSION_DATA}"
    obsolete_scan "${REPOSITORY_PATH}/${PATH_HTML}/"*".${EXTENSION_HTML}"
    (( INDENT -= 2 ))
}

# Check updates directory for packages, add them if they already exist in the
# active repository
function update_automatic
{

    [[ "$REPOSITORY_UPDATE" == "" ]] && return 0
    [[ ! -d "$REPOSITORY_UPDATE" ]] && return 0
    local i
    local first
    local seach
    local pkg
    local grp
    for i in "$REPOSITORY_UPDATE"/*.zip ; do
        [[ ! -f "$i" ]] && return 0
        [[ "$first" == "" ]] && {
            repository_protected && show_error 9 "Update packages found, but repository is read-only"
            first="nope"
            log "Automatically adding update packages"
            (( INDENT += 2 ))
        }
        pkg="${i##*/}"
        search=$(find_package "${pkg%.*}")
        if [[ "$search" == "" ]] ; then
            message "Skipped \`${pkg}' not in repository"
        else
            grp="${search%/*}"
            grp="${grp##*/}"
            check_group "$grp"
            package_add "$grp" "$i"
        fi
    done
    (( INDENT -= 2 ))
    log "adding updated packages finished."
}

# The minimum maintenance process to retain repository integrity
function update_structure
{
    HUSH=yes
    [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
    system_rm "${PATH_TEMP}/listing.txt"
    update_automatic
    update_filesystem
    rebuild_items DATA data
    rebuild_items INDEX index
    rebuild_main_index
    if [[ -e "${REPOSITORY_PATH}/listing.csv" ]] ; then
        if [[ "${REBUILD_INDEX}" != "" ]] || [[ "$FORCE" == "yes" ]] ; then
            rebuild_main_csv
        fi
    fi;
    create_template_files
    rebuild_items DATA data_html
    rebuild_items INDEX index_html
    rebuild_main_index_html
    if [[ -e "${REPOSITORY_PATH}/${PATH_HTML}/rss.xml" ]] ; then
        make_generic rss
    fi;
    HUSH=
}

# Do everything
function update_all
{
    update_structure
    obsolete_scan_all
}

function make_iso_test
{
    [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
    [[ ! -f "${REPOSITORY_PATH}/listing.csv" ]] && show_error 2 "Missing \`${REPOSITORY_PATH}/listing.csv' file"
    [[ "$FORCE" == "yes" ]] && return 0
    [[ ! -f "${REPOSITORY_PATH}/${1}" ]] && return 0
    local istamp=$(external STAT "${REPOSITORY_PATH}/${1}")
    local lstamp=$(external STAT "${REPOSITORY_PATH}/listing.csv")
    [[ $istamp -lt $lstamp ]] && return 0
    return 1
}

function make_iso
{
    local method="$1"
    local iso="$2"
    local img="$3"
    local files="$4"
    local cdfs="${PATH_TEMP}/cdrootfs.tmp"
    make_iso_test "$iso" || {
        message "$method CD \`$iso' is newer than \`listing.csv', skipped."
        return 0
    }
    log "Create $method CD ISO image \`$iso'"
    repository_protected && return 0
    system_rm "$cdfs"
    system_mkdir "$cdfs"

    (( INDENT += 2 ))

    local i
    local p
    local html
    local target
    local name
    local data

    [[ -d "${PATH_DATA}" ]] && {
        message "${PATH_DATA}/"
        system_mkdir "${cdfs}/${PATH_DATA}"
    }
    [[ -d "${PATH_HTML}" ]] && {
        message "${PATH_HTML}/"
        system_mkdir "${cdfs}/${PATH_HTML}"
    }
    for i in * ; do
        if [[ -f "$i" ]] ; then
            [[ "${i%.*}" == "listing" ]] && {
                message "$i"
                system_cp "$i" "${cdfs}/$i"
            }
        elif [[ -d "$i" ]] && [[ -f "$i/index.lst" ]] ; then
            message "$i/"
            system_mkdir "${cdfs}/$i"
            html="${PATH_HTML}/${PREFIX_GROUP}$i.${EXTENSION_HTML}"
            [[ -e "${html}" ]] && system_cp "${html}" "${cdfs}/${html}"
            (( INDENT += 2 ))
            for p in "${i}/"* ; do
                if [[ ! -d "$p" ]] ; then
                    name="${p#*/}"
                    if [[ -L "$p" ]] ; then
                        target=$(link_target "$p")
                        message "Latest version of \`$name' (${target##*/}) only."
                        system_cp "$target" "${cdfs}/${p}"
                    else
                        system_cp "$p" "${cdfs}/${p}"
                        [[ "${FILE_IGNORE//;${name};/}" == "$FILE_IGNORE" ]] && {
                            data="${name%.*}.${EXTENSION_DATA}"
                            html="${name%.*}.${EXTENSION_HTML}"
                            [[ -e "${PATH_DATA}/${data}" ]] && system_cp "${PATH_DATA}/${data}" "${cdfs}/${PATH_DATA}/${data}"
                            [[ -e "${PATH_HTML}/${html}" ]] && system_cp "${PATH_HTML}/${html}" "${cdfs}/${PATH_HTML}/${html}"
                        }
                    fi
                fi
            done
            (( INDENT -= 2 ))
        fi;
    done
    (( INDENT -= 2 ))

    REPOSITORY_PATH="$cdfs"
    if [[ "$method" == "Alternate" ]] ; then
        REPOSITORY_TITLE=$(external ALTTITLE)
    else
        REPOSITORY_TITLE=$(external CDTITLE)
    fi
    REPOSITORY_URL="$cdfs"
    system_cd "$cdfs"
    log "Updating $method CD ISO image data and html files."

    system_rm "${PATH_TEMP}/listing.txt"
    update_filesystem
    rebuild_items DATA data
    # Not needed -- rebuild_items INDEX index
    # Not needed -- rebuild_main_index
    create_template_files
    rebuild_items DATA data_html
    rebuild_items INDEX index_html
    rebuild_main_index_html

    repository_select "$REPOSITORY" QUIET

    if [[ "$files" != "" ]] ; then
        tar -xzf "$files" >/dev/null || show_error 3 "Extracting tar/gz archive \`$files'"
    fi

    if [[ "$method" == "Alternate" ]] ; then
        system_mkdir "${cdfs}/ISOLINUX"
        system_cp "$img" "${cdfs}/ISOLINUX/boot.img"
    else
        system_cp "$img" "${cdfs}/boot.img"
    fi

    system_cd "$cdfs"
    system_rm "${PATH_TEMP}/$iso" "${PATH_TEMP}/cdrom.iso"  "${PATH_TEMP}/cdrom.sha"  "${PATH_TEMP}/cdrom.md5"

    if [[ "$method" == "Alternate" ]] ; then
        external ALTISO
    else
        external CDISO
    fi

    external MD5 "${PATH_TEMP}/cdrom.iso" >"${PATH_TEMP}/cdrom.md5"
    external SHA "${PATH_TEMP}/cdrom.iso" >"${PATH_TEMP}/cdrom.sha"
    system_touch "${PATH_TEMP}/cdrom."*
    system_rm "${REPOSITORY_PATH}/$iso" "${REPOSITORY_PATH}/${iso%.*}.md5" "${REPOSITORY_PATH}/${iso%.*}.sha"
    system_mv "${PATH_TEMP}/cdrom.iso" "${REPOSITORY_PATH}/$iso"
    system_mv "${PATH_TEMP}/cdrom.md5" "${REPOSITORY_PATH}/${iso%.*}.md5"
    system_mv "${PATH_TEMP}/cdrom.sha" "${REPOSITORY_PATH}/${iso%.*}.sha"
    system_rm "${cdfs}"

    log "$method CD ISO image \`$iso' complete."

    repository_select "$REPOSITORY" QUIET

}

function make_alt_iso
{
    make_iso "Alternate" "${FILE_ALT_ISO}" "${FILE_ALT_FLOPPY}" "${FILE_ALT_FILES}"
}

function make_cd_iso
{
    make_iso "Standard" "${FILE_CD_ISO}" "${FILE_CD_FLOPPY}" "${FILE_CD_FILES}"
}

function purge_all_data
{
    [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
    [[ "$FORCE" != "yes" ]] && show_error 3 "This will remove all utility created files from the \`$REPOSITORY_ID' repository. You are required to \`force' this action."

    log "Purge \`$REPOSITORY_ID' forced!"
    repository_protected && return 0
    (( INDENT += 2 ))
    if [[ -d "${PATH_HTML}" ]] ; then
        local first=
        local i
        local hold="$PWD"
        system_cd "${PATH_HTML}"
        for i in *.${EXTENSION_HTML} *.css *.${EXTENSION_XML} ; do
            [[ -e "$i" ]] && {
                [[ "$first" == "" ]] && log "Remove generated files from HTML directory \`${PATH_HTML}'"
                first=no
                log "Remove file \`$i'"
                system_rm "$i"
            }
        done
        system_cd "$hold"
    fi
    if [[ -d "${PATH_DATA}" ]] ; then
        local first=
        local i
        local hold="$PWD"
        system_cd "${PATH_DATA}"
        for i in *.${EXTENSION_DATA} ; do
            [[ -e "$i" ]] && {
                [[ "$first" == "" ]] && log "Remove generated files from DATA directory \`${PATH_DATA}'"
                first=no
                log "Remove file \`$i'"
                system_rm "$i"
            }
        done
        system_cd "$hold"
    fi
    local i
    for i in * ; do
        if [[ -e "$i" ]] ; then
            if [[ "${i%.*}" == "listing" ]]  ; then
                [[ "$first" == "" ]] && log "Removing file \`$i'"
                system_rm "${i}"
            elif [[ "${i%.*}" == "${FILE_CD_ISO%.*}" ]]  ; then
                [[ "$first" == "" ]] && log "Removing file \`$i'"
                system_rm "${i}"
            elif [[ "${i%.*}" == "${FILE_ALT_ISO%.*}" ]]  ; then
                [[ "$first" == "" ]] && log "Removing file \`$i'"
                system_rm "${i}"
            fi
        fi
    done
    local g
    for g in * ; do
        if [[ -d "$g" ]] ; then
            for i in "${g}/"* ; do
                if [[ -L "$i" ]] ; then
                    log "Remove link \`$i'"
                    system_rm "${i}"
                elif [[ -e "$i" ]] ; then
                    if [[ "${i%.*}" == "${g}/index" ]] ; then
                        log "Remove file \`$i'"
                        system_rm "${i}"
                    fi
                fi
            done
            ls -1d "${g}"/* >/dev/null 2>&1 || {
                log "Remove empty \`$g' directory"
                system_rm "${g}"
            }
        fi
    done
    (( INDENT -= 2 ))
}

function check_group
{
    if [[ ! -d "${REPOSITORY_PATH}/$1" ]] || [[ "${1}" == "${PATH_HTML}" ]] || [[ "${1}" == "${PATH_DATA}" ]] ; then
        if [[ "$FORCE" == "yes" ]] ; then
            log "ERROR WARNING (6) Invalid package group \`$1', ignored!"
        else
            message "Package groups in \`$REPOSITORY_ID' are:"
            (( INDENT += 2 ))
            for i in "${REPOSITORY_PATH}/"* ; do
                if [[ -d "$i" ]] && [[ "${i##*/}" != "${PATH_HTML}" ]] && [[ "${i##*/}" != "${PATH_DATA}" ]] ; then
                    message "${i##*/}"
                fi
            done;
            (( INDENT -= 2 ))
            show_error 6 "Invalid package group \`$1'"
        fi
    fi
    return 0
}

function extract_lsm
{
    local pkg=$(caseUpper "${1##*/}")
    unzip -Cc "${1}" "APPINFO/${pkg%.*}.lsm"| grep -a  -A 10000 -i "^Begin3" | grep -a  -B 10000 -i "^end">"${PATH_TEMP}/${pkg%.*}.LSM" || show_error "extracting LSM from \`$1'"

}

function extract_package
{
    system_rm "${PATH_TEMP}/PACKAGE"
    system_mkdir "${PATH_TEMP}/PACKAGE"
    local hold="$PWD"
    [[ "${1//\//}" != "$1" ]] && system_cd "${1%/*}"
    local fromp="$PWD"
    system_cd "${PATH_TEMP}/PACKAGE"
    unzip -q "${fromp}/${1##*/}" || show_error 9 "Package extraction error."
    system_cd "$hold"
}

function compress_package
{
    local hold="$PWD"
    system_cd "${PATH_TEMP}/PACKAGE"
    local pkg="$(caseLower ${1##*/})"
    local npkg="${PATH_TEMP}/temporary-${pkg%.*}.zip"
    zip -q -9 -r -k "${npkg}" * || show_error 11 "Archive compression error."
    system_mv "${npkg}" "${PATH_TEMP}/${pkg%.*}.zip"
    system_cd "$hold"
    system_rm "${PATH_TEMP}/PACKAGE"
}

function check_date
{
    [[ "${#1}" -ne 10 ]] && [[ "${#1}" -ne 12 ]] && return 1
    [[ "${1:4:1}" != "-" ]] && return 1
    [[ "${1:7:1}" != "-" ]] && return 1
    [[ "${#1}" -eq 12 ]] && [[ "${1:10:1}" != "." ]] && return 1
    local left=$(echo "${1:0:10}" | tr -d "[:digit:]")
    [[ "$left" != "--" ]] && return 1
    return 0
}

function package_add
{

    local cppkg="$2"
    local pkg=$(caseUpper "${2##*/}")
    system_rm "${PATH_TEMP}/${pkg%.*}.LSM"
    extract_lsm "$cppkg"
    clear_field_data
    populate_item_data "${PATH_TEMP}/${pkg%.*}.LSM" dumb
    message "Package \`$(caseLower ${pkg})'"

    (( INDENT += 2 ))

    [[ "$DATAFIELD_MODIFIEDDATE" == "" ]] && {
        extract_package "$cppkg"
        local lsm=$(find "${PATH_TEMP}/PACKAGE" | grep -a  -i "^${PATH_TEMP}/PACKAGE/APPINFO/${pkg%.*}.lsm")
        if [[ "$lsm" == "" ]] || [[ ! -f "$lsm" ]] ; then
            show_error 10 "Processing package"
        fi
        system_rm "$lsm" "${PATH_TEMP}/${pkg%.*}.zip"
        cat "${PATH_TEMP}/${pkg%.*}.LSM" | grep -a  -iv "^end" >"$lsm"
        echo "Modified-date: $(external DATEMOD).0" >>"$lsm"
        echo "end">>"$lsm"

        clear_field_data
        populate_item_data "${lsm}" dumb
        compress_package "$cppkg"
        local cppkg="${PATH_TEMP}/$(caseLower ${pkg%.*}).zip"
    }

    system_rm "${PATH_TEMP}/${pkg%.*}.LSM"

    check_date "$DATAFIELD_ENTEREDDATE" || show_error 7 "Invalid Entered-Date format \`$DATAFIELD_ENTEREDDATE' (requires YYYY-MM-DD)"
    check_date "$DATAFIELD_MODIFIEDDATE" || show_error 7 "Invalid Modified-Date format \`$DATAFIELD_MODIFIEDDATE' (requires YYYY-MM-DD[.N])"

    if [[ ! true ]] ; then
        [[ "$DATAFIELD_TITLE" == "" ]] && show_error 8 "Missing package \`Title'"
        [[ "$DATAFIELD_VERSION" == "" ]] && show_error 8 "Missing package \`Version'"
        [[ "$DATAFIELD_COPYINGPOLICY" == "" ]] && show_error 8 "Missing package \`Copying-policy'"
        [[ "$DATAFIELD_DESCRIPTION" == "" ]] && show_error 8 "Missing package \`Description'"

        local list="$DATAFIELDS"
        local item=
        while [[ "$list" != "" ]] ; do
            item="${list%%;*}"
            list="${list#*;}"
            [[ "$list" == "$item" ]] && list=""
            item="DATAFIELD_${item:1}"
            message "LSM:${item#*_}=\`${!item}'"
        done
    fi
    local dpkg=$(caseLower "${cppkg##*/}")

    local search=$(find_package "${dpkg%.*}")

    local search="${search:$(( ${#REPOSITORY_PATH} + 1 )) }"
    local sdir="${search%/*}"

    local sha
    local sha1=$(external SHA "${cppkg}")
    local sha2=$(external SHA "${2}")

    local skip=no
    if [[ "$search" != "" ]] && [[ "$sha1" != "" ]] ; then
        if [[ -d "${REPOSITORY_PATH}/$sdir/${dpkg%.*}" ]] ; then
            local i
            for i in "${REPOSITORY_PATH}/$sdir/${dpkg%.*}/"* ; do
                sha=$(external SHA  "$i")
                if [[ "$sha" == "$sha1" ]] || [[ "$sha" == "$sha2" ]] ; then
                    dpkg="${i:$(( ${#REPOSITORY_PATH} + 1 ))}"
                    skip=yes
                fi
            done
        else
            local sha=$(external SHA "${REPOSITORY_PATH}/$sdir/${dpkg}")
            if [[ "$sha" == "$sha1" ]] || [[ "$sha" == "$sha2" ]] ; then
                skip=yes
                dpkg="$sdir/${dpkg}"
            fi
        fi
    fi

    system_rm "${PATH_TEMP}/${pkg%.*}.LSM" "${PATH_TEMP}/PACKAGE"

    if [[ "$skip" == "yes" ]] ; then
        message "Existing package \`${dpkg}' is identical. Skipped."
    else
        local vermode=none
        if [[ "$search" != "" ]] && [[ "$sdir" != "$1" ]] ; then
            show_error 12 "Package already exists in group \`${sdir%/*}' cannot put in group \`$1'"
        elif [[ "$search" != "" ]] && [[ ! -d "${REPOSITORY_PATH}/$1/${dpkg%.*}" ]] ; then
            local vermode=2
        elif [[ -d "${REPOSITORY_PATH}/$1/${dpkg%.*}" ]] ; then
            local vermode=1
        else
            log "Adding new packaged \`${dpkg}' to group \`$1'"
            system_cp "${cppkg}" "${REPOSITORY_PATH}/$1/${dpkg}"
            system_rm "${cppkg}" "${2}"
        fi;

        local myver="$DATAFIELD_VERSION"

        if [[ "$vermode" == "2" ]] ; then
            log "Versioning package \`${dpkg%.*}' in group \`$1'"
            extract_lsm "${REPOSITORY_PATH}/$1/${dpkg}"
            clear_field_data
            populate_item_data "${PATH_TEMP}/${pkg%.*}.LSM" dumb
            local vermode=1
            local tname=$(safe_filename "$DATAFIELD_VERSION" | tr -d \''|\,`~+=:;"@!#$%^&()[]{}<>' | tr ' -.\/' '_' | tr -s '_' | tr "[:upper:]" "[:lower:]")".${dpkg##*.}"
            system_mkdir "${REPOSITORY_PATH}/$1"
            system_mkdir "${REPOSITORY_PATH}/$1/${dpkg%.*}"
            system_mv "${REPOSITORY_PATH}/$1/${dpkg}" "${REPOSITORY_PATH}/$1/${dpkg%.*}/${tname}"
        fi

        if [[ "$vermode" == "1" ]] ; then
            local tname=$(safe_filename "$myver" | tr -d \''|\,`~+=:;"@!#$%^&()[]{}<>' | tr ' -.\/' '_' | tr -s '_' | tr "[:upper:]" "[:lower:]")".${dpkg##*.}"
            local tname="${REPOSITORY_PATH}/$1/${dpkg%.*}/$tname"
            local dpkg="$tname"
            local j=0
            while [[ -e "$dpkg" ]] ; do
                local dpkg="${tname%.*}-${j}.${tname##*.}"
                (( j++ ))
            done
            local tname="${dpkg##*/}"
            log "Adding versioned package \`${pkg%.*}/${tname%.*}' in group \`$1'"
            system_cp "${cppkg}" "${dpkg}"
            system_rm "${cppkg}" "${2}"
        fi

        system_rm "${PATH_TEMP}/${pkg%.*}.LSM"
        clear_field_data
    fi

    [[ "$cppkg" != "$2" ]] && system_rm "$cppkg"

    (( INDENT -= 2 ))
}

function package_edit
{
    local spkg="$1"
    local pkg="$(caseLower ${spkg##*/})"

    if [[ -L "$spkg" ]] ; then
        local spkg=$(link_target "$spkg")
    fi

    extract_package "$spkg"

    local lsm=$(ls -a1d "$PATH_TEMP/PACKAGE/"* | grep -a  -i "$PATH_TEMP/PACKAGE/APPINFO")
    local lsm=$(ls -a1d "$lsm/"* | grep -a  -i "$lsm/${pkg%.*}.lsm")

    system_cp "$lsm" "$PATH_TEMP/${lsm##*/}"

    local edited=no

    external EDIT "$lsm"

    diff "$lsm" "$PATH_TEMP/${lsm##*/}" >/dev/null || local edited=yes

    if [[ "$edited" == "yes" ]] ; then
        compress_package "$PATH_TEMP/$pkg"
        system_rm "$spkg"
        system_cp "$PATH_TEMP/$pkg" "$spkg"
        system_rm "$PATH_TEMP/$pkg"
        if [[ "$spkg" == "$1" ]] ; then
            log "Changes to package \`$pkg' applied."
        else
            log "Changes to package \`${pkg%.*}' (${spkg##*/}) applied."
        fi
    else
        if [[ "$spkg" == "$1" ]] ; then
            message "No changes made to package \`$pkg' detected."
        else
            message "No changes made to package \`${pkg%.*}' (${spkg##*/}) detected."
        fi
    fi

    system_rm "$PATH_TEMP/${lsm##*/}" "$PATH_TEMP/$pkg"

}

function make_generic
{
    local ext=${EXTENSION_HTML}
    [[ "$1" == "rss" ]] && local ext=${EXTENSION_XML}

    [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"

    local name="${1}"
    if [[ "${REBUILD_INDEX}" == "" ]] && [[ "$FORCE" != "yes" ]] && [[ -e "${REPOSITORY_PATH}/${PATH_HTML}/${name}.${ext}" ]] ; then
        local cstamp=$(external STAT "${REPOSITORY_PATH}/${PATH_HTML}/${name}.${ext}")
        local lstamp=$(external STAT "${REPOSITORY_PATH}/listing.csv")
        if [[ $lstamp -lt $cstamp ]] ; then
            [[ "$HUSH" == "" ]] && message "Existing \`${name}.${ext}' is newer than \`listing.csv', skipped."
            return 0
        fi
    fi

    if [[ "$HUSH" == "" ]] || [[ $lstamp -lt $cstamp ]] || [[ "${REBUILD_INDEX}" != "" ]] || [[ "$FORCE" == "yes" ]]; then
        log "Make generic \`${name}.${ext}' file."
    fi
    [[ ! -f "${REPOSITORY_PATH}/listing.csv" ]] && show_error 1 "Repository \`$REPOSITORY_ID' missing required \`listing.csv'"
    repository_protected && return 0
    create_template_files
    system_rm "${PATH_TEMP}/${name}.tmp"

    repository_select "$REPOSITORY_ID" QUIET

    (( INDENT += 2 ))

    process_html HTML "$1" >"${PATH_TEMP}/${name}.tmp"

    place_file "${PATH_TEMP}/${name}.tmp" "${REPOSITORY_PATH}/${PATH_HTML}/${name}.${ext}"

    (( INDEND-- ))
    (( INDEND-- ))
    return 0
}

# Script main parameter processing routine
function process_main
{
    while [[ ! ${#*} -eq 0 ]] ; do
        case "${1}" in
        about)
            show_about
            ;;
        help)
            show_help
            ;;
        force)
            FORCE=yes
            ;;
        noforce)
            FORCE=no
            ;;
        quiet)
            QUIET=true
            ;;
        verbose)
            QUIET=false
            ;;
        update)
            update_structure
            ;;
        cleanup)
            obsolete_scan_all
            ;;
        all)
            update_all
            ;;
        csv)
            rebuild_main_csv
            ;;
        make)
            shift
            make_generic "$1"
            ;;
        rss)
            make_generic rss
            ;;
        repo|repository)
            shift
            repository_select "$1"
            ;;
        activate|active|act)
            [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
            REPOSITORY="$REPOSITORY_ID"
            message "Repository \`$REPOSITORY_ID' set as the default active."
            config_write
            ;;
        available|avail)
            repository_list
            ;;
        comparison|compare|comp)
            shift
            repository_compare "$1"
            ;;
        altiso)
            make_alt_iso
            ;;
        cdiso)
            make_cd_iso
            ;;
        iso)
            make_cd_iso
            ;;
        purge)
            purge_all_data
            ;;
        add)
            [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
            shift
            local id="$1"
            check_group "$id"
            system_cd "$START_PATH"
            if [[ ! -f "$2" ]] ; then
                show_error 5 "Missing package file \`$2'"
            else
                while [[ -f "$2" ]] ; do
                    shift
                    package_add "$id" "$1"
                done
            fi
            repository_select "$REPOSITORY_ID" QUIET
            ;;
        edit)
            [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
            local try=yes
            system_cd "$START_PATH"
            while [[ "$try" != "no" ]] ; do
                if [[ -f "$2" ]] ; then
                    package_edit "$2"
                    shift
                    local try=maybe
                else
                    local search=$(find_package "${2}")
                    if [[ -f "$search" ]] ; then
                        package_edit "$search"
                        shift
                        local try=maybe
                    elif [[ "$try" == "yes" ]] ; then
                        show_error 13 "File or package \`$2' not found."
                    else
                        local try=no
                    fi
                fi
            done
            repository_select "$REPOSITORY_ID" QUIET
            ;;
        move)
            [[ "$REPOSITORY_ID" == "" ]] && repository_select "${REPOSITORY}"
            shift
            local id="$1"
            check_group "$id"
            local try=yes
            while [[ "$try" != "no" ]] ; do
                local search=$(find_package "${2}")
                if [[ -f "$search" ]] ; then
                    local search="${search:$(( 1 + ${#REPOSITORY_PATH}))}"
                    if [[ "${search%%/*}" != "$id" ]] ; then
                        local name="${search##*/}"
                        system_rm "${search%%/*}/index.lst"
                        system_rm "${search%%/*}/index.gz"
                        system_rm "${id}/index.lst"
                        system_rm "${id}/index.gz"
                        system_rm "${PATH_DATA}/${name%%.*}"*${EXTENSION_DATA}
                        system_rm "${PATH_HTML}/${name%%.*}"*${EXTENSION_HTML}
                        if [[ -L "$search" ]] ; then
                            system_rm "$search"

                            system_mv "${search%.*}" "${id}/${name%.*}"
                            local hold="$PWD"
                            system_cd  "${id}"
                            local latest="$(file_latest ${name%.*} 2>/dev/null)"
                            local latest="${latest#*/}"
                            system_ln "${name%.*}/${latest}" "${name}"
                            system_cd "$hold"
                            log "Moved versioned package \`${name}' to \`${id}'"
                        else
                            system_mv "$search" "$id/$name"
                            log "Moved single package \`${name}' to \`${id}'"
                        fi
                    fi
                    shift
                    local try=maybe
                elif [[ "$try" == "yes" ]] ; then
                    show_error 13 "Package \`$2' not found"
                else
                    local try=no
                fi
            done
            ;;
        *)
            show_error 1 "invalid option \`${1}'"
        esac
        shift
    done
    [[ "$ANYLOGGING" == true ]] && log "finished." || message "finished."
}

# Platform configuration routines
[[ "$(uname)" == "Darwin" ]] && MacOSX=yes || MacOSX=
[[ "$(uname)" == "Linux" ]]  && Linux=yes  || Linux=

# Initialize configuration settings
SELF=$(ls -ald "${0}")
SELF="${SELF##* -> }"
SELF=$(path_expand "$SELF")
START_PATH="$PWD"

config_default
config_read
# config_write

SPACES=
STARS=
while [[ INDENT -gt 0 ]] ; do
    SPACES="$SPACES "
    STARS="${STARS}*"
    (( INDENT-- ))
done

# Main script startup
if [[ ${#*} -eq 0 ]] ; then
    show_help
else
    process_main "${@}" && exit 0 || exit $?
fi
